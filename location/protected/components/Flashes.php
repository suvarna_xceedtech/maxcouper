<?php 
/* 

*/
 
class Flashes extends CWidget {
	public function run() {
		Yii::app()->clientScript->registerScript(
		   'myHideEffect',
		 	'$("#exampleModal1").modal("show");
			 $("#ok_btn").click(function(){
			 	$("#exampleModal1").modal("hide")
			 });',
		   CClientScript::POS_READY
		);
	//echo "GI"; 
	$flashMessages = Yii::app()->user->getFlashes();
	if ($flashMessages) { ?>
	<div class="modal fade" id="exampleModal1" tabindex="-1" style="padding-top:40px;" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<?php  foreach($flashMessages as $key => $message) 
			{  ?>
				<div class="alert alert-<?php  echo $key;?> alert-dismissable">
			<?php 		if($key=="success")
						echo '<i class="fa fa-check"></i>';
					else if($key=="danger")
						echo '<i class="fa fa-ban"></i>';
					else if($key=="info")
						echo '<i class="fa fa-info"></i>';
					else if($key=="warning")
						echo '<i class="fa fa-warning"></i>';
			?>
					
					<button type="button"  id="ok_btn"  class="close" data-dismiss="alert" aria-hidden="true">×</button>
					 <?php  echo $message ?>
				</div>
			<?php 
				//echo '<div class="flash-' . $key . ' shadow">' . $message . "";?>
			<?php  } ?>
		</div>
	</div>		
<?php 	}	
     }
 }
?>
