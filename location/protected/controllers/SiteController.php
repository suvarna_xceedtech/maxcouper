<?php 

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		
		if(Yii::app()->user->isGuest)
		{	 
			$this->redirect(array('login'));
		}
		else
			//echo "Hi a";die;
			$this->render('index');
	}

	public function actionAboutus()
	{
		
			$this->render('about');
	}
	public function actionLocation()
	{
		$this->render('contact');
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		//echo "Hi";die;
		Yii::app()->user->setState('menu_id',0);
		$model=new LoginForm;
		if(Yii::app()->user->isGuest)
		{
			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST['LoginForm'];
				if($model->validate() && $model->login())
				{	
					$userData=Users::model()->findByPk(Yii::app()->user->getId());
					$emp_id=$userData['id'];
				
					$this->redirect(Yii::app()->user->returnUrl);
				}
			}
		
			$this->render('login',array('model'=>$model));
		}
		else
		{
			$this->render('index');
		}
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
