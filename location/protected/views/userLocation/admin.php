<?php 
/* @var $this UserLocationController */
/* @var $model UserLocation */

$this->breadcrumbs=array(
	'User Locations'=>array('admin'),
	'Manage',
);


?>
<center>

<?php  echo CHtml::link('Add New',array('clients/Create'),array('class'=>'btn btn-primary')); ?>
</center>
<?php  $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-location-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'latitude',
		'longitude',
		'address',
		'city',
		/*
		'battery',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
