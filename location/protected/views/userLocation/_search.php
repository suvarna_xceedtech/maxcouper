<?php 
/* @var $this UserLocationController */
/* @var $model UserLocation */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php  $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php  echo $form->label($model,'id'); ?>
		<?php  echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php  echo $form->label($model,'user_id'); ?>
		<?php  echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php  echo $form->label($model,'latitude'); ?>
		<?php  echo $form->textField($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php  echo $form->label($model,'longitude'); ?>
		<?php  echo $form->textField($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php  echo $form->label($model,'address'); ?>
		<?php  echo $form->textField($model,'address',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php  echo $form->label($model,'city'); ?>
		<?php  echo $form->textField($model,'city',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php  echo $form->label($model,'battery'); ?>
		<?php  echo $form->textField($model,'battery',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php  echo CHtml::submitButton('Search'); ?>
	</div>

<?php  $this->endWidget(); ?>

</div><!-- search-form -->