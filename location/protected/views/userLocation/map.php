<style>
    input[type="radio"] {
   
    margin-left: 10px;
</style>
<?php 
/* @var $this UserLocationController */
/* @var $model UserLocation */

$this->breadcrumbs=array(
  'User Locations'=>array('map'),
  $model->id,
);
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
<style type="text/css">
    
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDQffT82WtWIDw_P59J5Wp8XZ4s2otQP8&sensor=false"></script> 

<script type="text/javascript" src="js/map-font-icons.js"></script> 
 
 <?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'movie-master-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'), 

)); ?>
<div class="col-lg-12">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-10 col-md-offset-1" style="background:#eee;padding:10px">
                    <center>
                        <?php echo CHtml::radioButtonList('city_code','',array('User'=>'User','City'=>'City'),array(
                    'labelOptions'=>array('style'=>'display:inline'), // add this code
                    'separator'=>'',
                    'onclick'=>'showDiv(this.value);'
                )); ?>
                    </center>
                    
                    </div>
            </div>
            <div class="showDiv" style="display:none">
                 <fieldset>
                <p class="note">Fields with <span class="required">*</span> are required.</p>
                <div class="row">
                <div class="ifUser">
                    <div class="control-group col-md-4">
                        <?php echo $form->labelEx($model,'user_id'); ?>
                        <div class="controls">
                           <?php  $data=Users::model()->getUserList();
                                $this->widget('ext.select2.ESelect2',array(
                                'model'=>$model,
                                'attribute'=>'user_id',
                                'data'=>$data,
                                'options'=>array(
                                    'placeholder'=>'-- Select User --',
                                    'allowClear'=>true,
    
                                    
                                 ),
                                'htmlOptions'=>array(
                                 'style'=>'width:100%;' ,
                                  'multiple'=>'multiple'
                                 //'onchange'=>'ShowMap(this)'      
                                ),
                                )); 
                            ?>
                           <?php echo $form->error($model,'user_id'); ?>
                        </div>
                    </div>
                </div>
                <div class="ifCity">
                	<div class="col-md-3">
                		<?php  echo $form->labelEx($model,'city'); ?>
                
                		<?php  $users=City::model()->getCity();
                				$this->widget('ext.select2.ESelect2',array(
                					'name'=>'city',
                					'attribute'=>'city',
                					'data'=>$users,
                					'options'=>array(
                						'placeholder'=>'-- Select City --',
                						'allowClear'=>true,
                					),
                					'htmlOptions'=>array( 
                					
                					'style'=>'width:100%',
                
                					),
                					)); 
                
                		?>
                	</div>
                </div>
                <div class="col-md-2">
                <?php  echo $form->labelEx($model,'from_date'); ?>
                <?php         
                    $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'name'=>'from_date',
                          'attribute'=>'from_date',
                           'options'=>array(
                        
                        'showAnim'=>'fold',
                        'showButtonPanel'=>true,
                        'autoSize'=>true,
                        'dateFormat'=>'dd-mm-yy',
                    'changeYear'=>true,
                    'changeMonth'=>true,
                      ),
                    
                    ));
                        ?>
              </div>
              <div class="col-md-2">
                <?php  echo $form->labelEx($model,'created_at'); ?>
                <?php         
                    $form->widget('zii.widgets.jui.CJuiDatePicker', array(
                  'model'=>$model,
                  'name'=>'created_at',
                        'attribute'=>'created_at',
                      'options'=>array(
                      
                      'showAnim'=>'fold',
                      'showButtonPanel'=>true,
                      'autoSize'=>true,
                      'dateFormat'=>'dd-mm-yy',
                  'changeYear'=>true,
                  'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                    'value'=>"",
                    ),  
                    ));
               ?>
              </div>
              <div class="col-md-2 mt-10">
                <div class="mt-14">
                    <?php  echo CHtml::submitButton('Search', array('onclick'=>'return getData(1)')); ?>
                    <?php //echo CHtml::submitButton('Distance Report', array('onclick'=>'return getData(2)')); ?>
                </div>
      
              </div>
            </div>
          </fieldset>
            </div>
           
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<?php $this->endWidget(); ?>
<div class="row mt-10" id="mapdata">
    <div id="map_canvas"></div>
</div>
<div class="row mt-10">
    <div id="resultTbl" style="width:100%;overflow-x:auto">
    
    </div>
</div>
<script type="text/javascript">
    $(function() {
 
  var CoorPath="";
  var  userCoor="";
  var coorArr="";
  var count=0;
  var result="";
  var pathArr=[];
  var coor="";
  $.ajax({
        type:'POST',
      
        url:"<?php echo Controller::createUrl('API/getCurrentLocation') ?>",
        success:function(items)
        {
          var myarry = JSON.stringify(items);
          var stringify = JSON.parse(items);
          result= stringify;
          count= stringify.length;
          if(stringify.length>0)
          {
            userCoor = stringify;
            for(var index in stringify) {
              coor=new google.maps.LatLng(stringify[0][1], stringify[0][2])
              pathArr.push(new google.maps.LatLng(stringify[index][1], stringify[index][2])); 
            }
            var map;
            var mapOptions = { center: coor, zoom: 10,
              mapTypeId: google.maps.MapTypeId.ROADMAP};
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
          var userCoorPath = pathArr;
          // var userCoordinate = new google.maps.Polyline({
          // path: userCoorPath,
          // strokeColor: "#FF0000",
          // strokeOpacity: 1,
          // strokeWeight: 2
          // });
         // userCoordinate.setMap(map);
           var infowindow = new google.maps.InfoWindow({
                disableAutoPan: false,
                maxWidth: 300,
                zIndex: null,
                closeBoxMargin: "12px 4px 2px 2px",
                infoBoxClearance: new google.maps.Size(1, 1)
            });
          var marker, i;
          for (i = 0; i < count; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(result[i][1], result[i][2]),
              map: map,
              icon: {
                        path: MAP_PIN,
                        fillColor: result[i][3],
                        fillOpacity: 1,
                        strokeColor: '#ffffff',
                        strokeWeight: 0
                    },
              //icon:"http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_yellow.png"
            });

            google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
              return function() {
                infowindow.setContent(userCoor[i][0]);
                infowindow.open(map, marker);
              }
            })(marker, i));
             google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
                  return function() {
                    
                    infowindow.close(map, marker);
                  }
                })(marker, i));
          }
          google.maps.event.addDomListener(window, 'load', initialize);
          }
          else
          {
            $("#map_canvas").html("No records found");
          }

        }
      });

});
  function getData(flag)
  {
      var reporttype=localStorage.getItem("selected_val");
      var user_id=$("#UserLocation_user_id").val();
      var date_of_requirement= $("#from_date").val();
      var created_at=$("#created_at").val();
      var city=$("#city").val();

    if(flag==1)
    {
        var CoorPath="";
        var userCoor="";
        var coorArr="";
        var count=0;
        var result="";
        var pathArr=[];
        var coor="";
       
        $.ajax({
            type:'POST',
            data:{user_id:user_id, date_of_requirement: date_of_requirement, created_at: created_at, reporttype:reporttype, city:city },
            url:"<?php echo Controller::createUrl('API/getUsersLocations') ?>",
            success:function(items)
            {
                $( "#resultTbl" ).hide();
                var myarry = JSON.stringify(items);
              var stringify = JSON.parse(items);
              result= stringify;
              count= stringify.length;
              if(stringify.length>0)
              {
                userCoor = stringify;
                for(var index in stringify) {
                  coor=new google.maps.LatLng(stringify[0][1], stringify[0][2])
                  pathArr.push(new google.maps.LatLng(stringify[index][1], stringify[index][2])); 
                }
                var map;
                var mapOptions = { center: coor, zoom: 20,
                  mapTypeId: google.maps.MapTypeId.ROADMAP};
                map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
              var userCoorPath = pathArr;
              // var userCoordinate = new google.maps.Polyline({
              // path: userCoorPath,
              // strokeColor: "#FF0000",
              // strokeOpacity: 1,
              // strokeWeight: 2
              // });
              //userCoordinate.setMap(map);
                 var infowindow = new google.maps.InfoWindow({
            
                    disableAutoPan: false,
                    maxWidth: 300,
                    zIndex: null,
                    closeBoxMargin: "12px 4px 2px 2px",
                    infoBoxClearance: new google.maps.Size(1, 1)
                });
        
              var marker, i;
              for (i = 0; i < count; i++) {  
                marker = new google.maps.Marker({
                  position: new google.maps.LatLng(result[i][1], result[i][2]),
                  map: map,
                  icon: {
                            path: 'M-10,0a10,10 0 1,0 20,0a10,10 0 1,0 -20,0',
                            fillColor: result[i][3],
                            fillOpacity: 1,
                            strokeColor: result[i][4],
                            strokeWeight: 10
                        },
                });
    
                google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                  return function() {
                    infowindow.setContent(userCoor[i][0]);
                    infowindow.open(map, marker);
                  }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
                  return function() {
                    
                    infowindow.close(map, marker);
                  }
                })(marker, i));
              }
              google.maps.event.addDomListener(window, 'load', initialize);
              }
              else
              {
                $("#map_canvas").html("No records found");
              }
    
            }
          });
    }
   
    else
    {
        $( "#example-result" ).empty();
        if(user_id=="")
        {
            alert("Please select atleast on user");
        }
        else if(date_of_requirement=="")
        {
            alert("Please select from date");
        }
        else
        {
            $.ajax({
				type:'POST',
				data:{user_id:user_id, date_of_requirement: date_of_requirement,created_at: created_at },
				url:"<?php echo Controller::createUrl('API/getDistanceDataReport') ?>",
				success:function(data)
				{					
				    $( "#mapdata" ).hide();
					$( "#resultTbl" ).html(data );
					$('#example-result').DataTable( {
                	"pageLength": 10,
                	"searching": false,
                	"lengthChange": false,
                	"scrollY":"250px",
                	"scrollX":true,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'pdfHtml5',
                            text:      '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            className: 'btn btn-success',
                            orientation: 'landscape',
                            pageSize: 'LEGAL'
                        }
                    ]
                });

				}
			});
        
        }
		
    }
    return false;
   
  }
 function showDiv(val)
{
localStorage.setItem("selected_val",val);
    if(val=="User")
    {
        $(".showDiv").show();
        $(".ifUser").show();
        $(".ifCity").hide();
    }
    else if(val=="City")
    {
        $(".showDiv").show();
        $(".ifUser").hide();
        $(".ifCity").show();
    }
    else
    {
        alert("Report ttype is not selected");
    }
}
</script>
 <script>
// function changemap(user_id)
// {
//   var CoorPath="";
//   var  userCoor="";
//   var coorArr="";
//   var count=0;
//   var result="";
//   var pathArr=[];
//   var coor="";
//   $.ajax({
//         type:'POST',
//         data: {id: user_id},
//         url:"<?php echo Controller::createUrl('API/getLocation') ?>",
//         success:function(items)
//         {
//           var myarry = JSON.stringify(items);
//           var stringify = JSON.parse(items);
//           result= stringify;
//           count= stringify.length;
//           if(stringify.length>0)
//           {
//             userCoor = stringify;
//             for(var index in stringify) {
//               coor=new google.maps.LatLng(stringify[0][1], stringify[0][2])
//               pathArr.push(new google.maps.LatLng(stringify[index][1], stringify[index][2])); 
//             }
//             var map;
//             var mapOptions = { center: coor, zoom: 20,
//               mapTypeId: google.maps.MapTypeId.ROADMAP};
//             map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
//          // var userCoor = [coorArr];
//           var userCoorPath = pathArr;
//           var userCoordinate = new google.maps.Polyline({
//           path: userCoorPath,
//           strokeColor: "#FF0000",
//           strokeOpacity: 1,
//           strokeWeight: 2
//           });
//           userCoordinate.setMap(map);
//           var infowindow = new google.maps.InfoWindow();
//           var marker, i;
//           for (i = 0; i < count; i++) {  
//             marker = new google.maps.Marker({
//               position: new google.maps.LatLng(result[i][1], result[i][2]),
//               map: map,
//               icon: {
//                         path: SQUARE,
//                         fillColor: '#fab529',
//                         fillOpacity: 1,
//                         strokeColor: '',
//                         strokeWeight: 0
//                     },
//               //icon:"http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_yellow.png"
//             });

//             google.maps.event.addListener(marker, 'click', (function(marker, i) {
//               return function() {
//                 infowindow.setContent(userCoor[i][0]);
//                 infowindow.open(map, marker);
//               }
//             })(marker, i));
//           }
//           google.maps.event.addDomListener(window, 'load', initialize);
//           }
//           else
//           {
//             $("#map_canvas").html("No records found");
//           }

//         }
//       });
// }
</script>
