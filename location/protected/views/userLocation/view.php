<?php 
/* @var $this UserLocationController */
/* @var $model UserLocation */

$this->breadcrumbs=array(
	'User Locations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserLocation', 'url'=>array('index')),
	array('label'=>'Create UserLocation', 'url'=>array('create')),
	array('label'=>'Update UserLocation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserLocation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserLocation', 'url'=>array('admin')),
);
?>

<h1>View UserLocation #<?php  echo $model->id; ?></h1>

<?php  $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'latitude',
		'longitude',
		'address',
		'city',
		'battery',
	),
)); ?>
