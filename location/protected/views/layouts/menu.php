
<ul class="sidebar-menu">
	<li class="treeview">
		<a class="dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-angle-double-right"></i>Contact Management&nbsp;<i class="fa fa-angle-down pull-right"></i>
		</a>
		<ul class="treeview-menu" role="menu">
			<li><a href="index.php?r=AddressData/admin"><i class="fa fa-users"></i>&nbsp;Contacts</a></li>
			<li><a href="index.php?r=AssociateTypeMaster/admin"><i class="fa fa-th"></i>&nbsp;Category </a></li>
			<li><a href="index.php?r=AssociateSubtypeMaster/admin"><i class="fa fa-th"></i>&nbsp;Sub Category </a></li>
			<li><a href="index.php?r=AddressData/PrintLabels"><i class="fa fa-print"></i>&nbsp;Print Labels </a></li>
			<li><a href="index.php?r=country/admin"><i class="fa fa-globe"></i>&nbsp;Country </a></li>
			<li><a href="index.php?r=state/admin"><i class="fa fa-globe"></i>&nbsp;State </a></li>
			<li><a href="index.php?r=DistrictMaster/admin"><i class="fa fa-map-marker"></i>&nbsp;District</a></li>
			<li><a href="index.php?r=TalukaMaster/admin"><i class="fa fa-map-marker"></i>&nbsp;Tahasil</a></li>	
			<li><a href="index.php?r=AddressData/import"><i class="fa fa-upload"></i>&nbsp;Import Contacts</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-double-right"></i>Document Management&nbsp;<i class="fa fa-angle-down pull-right"></i>
		</a>
		<ul class="treeview-menu" role="menu">
			<li><a href="index.php?r=estore/admin"><i class="fa fa-upload"></i>&nbsp;Document Management</a></li>
			<!--<li><a href="index.php?r=estore/ManualSearch"><i class="fa fa-search"></i>&nbsp;Search</a></li>-->
			<li><a href="index.php?r=DocumentTypeMaster/admin"><i class="fa fa-folder"></i>&nbsp;Document Type</a></li>
			<li><a href="index.php?r=DocumentMaster/admin"><i class="fa fa-folder-open"></i>&nbsp;Document SubType</a></li>
		</ul>
	</li>		
	<li class="treeview">
		<a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-double-right"></i>Administration Master&nbsp;<i class="fa fa-angle-down pull-right"></i>
		</a>
		<ul class="treeview-menu" role="menu">
			<li><a href="index.php?r=EmployeeMaster/admin"><i class="fa fa-users"></i>&nbsp;Employees</a></li>
			<li><a href="index.php?r=UserMaster/admin"><i class="fa fa-user"></i>&nbsp;User Master</a></li>
			<li><a href="index.php?r=Department/admin"><i class="fa fa-sitemap"></i>&nbsp;Department</a></li>
			<li><a href="index.php?r=DesignationMaster/admin"><i class="fa fa-th"></i>&nbsp;Designations</a></li>
			<li><a href="index.php?r=MenuMaster/admin"><i class="fa fa-book"></i>&nbsp;Menu Master</a></li>
			<li><a href="index.php?r=UserMenuAccessMaster/admin"><i class="fa fa-lock"></i>&nbsp;User Menu Access</a></li>
		</ul>
	</li>
	<!--<li><a href="javascript:void(0)"><i class="fa fa-th"></i>&nbsp;Project Management</a></li>-->
 </ul>
