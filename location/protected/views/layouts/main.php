<!DOCTYPE html>
	<?php  if(Yii::app()->user->isGuest)
	{
		echo "<html class='bg-login'>";
	}
	else
	{
		echo "<html>";
	}
	?>
    	<head>
        <meta charset="UTF-8">
        <title>Carryking</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/plugins/social-buttons.css" rel="stylesheet">
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="css/plugins/timeline.css" rel="stylesheet">
        <link href="css/smartech.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/animate.css" rel="stylesheet">
        <link href="js/plugins/dataTables/jquery.dataTables.min.css" rel="stylesheet">
        <link href="js/plugins/dataTables/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
	<?php //if(!Yii::app()->user->isGuest) {
		include('master.php');
	//}
	?>
	<?php  Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <script src="js/bootstrap.min.js"></script>
		<script src="js/plugins/metisMenu/metisMenu.min.js"></script>
		<script src="js/smartech.js"></script>
		<script src="js/plugins/dataTables/jquery.dataTables.min.js"></script>
        <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src= "js/plugins/dataTables/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src= "js/plugins/dataTables/jszip.min.js"></script>

        <script type="text/javascript" src= "js/plugins/dataTables/pdfmake.min.js"></script>
		<script type="text/javascript" src= "js/plugins/dataTables/vfs_fonts.js"></script>
		<script type="text/javascript" src= "js/plugins/dataTables/buttons.html5.min.js"></script>
		<script type="text/javascript" src= "js/plugins/dataTables/buttons.print.min.js"></script>

        <!-- Sparkline JavaScript -->
        <script src="js/plugins/sparkline/sparkline.js"></script>
	<script>
	$(document).ready(function(){
		$('.grid-view .filters input[type=text]').attr("class","form-control");
		//$('.grid-view .filters select').attr("data-live-search","true");
		$('.table > tbody > tr > td.button-column').css({
			   'padding' : '0px !important',
			   'padding-top' : '11px !important',
			  });
		$('.table > thead > tr > th').css({'padding' : '0px !important'});
		//$('.grid-view .filters select').selectpicker();	
		$('select').not('.not-applyclass').attr("class","form-control");
		$('input[type=button]').attr("class","btn btn-lg btn-success btn-block");
		$('input[type=submit]').not('.login').attr("class","btn btn-lg btn-success btn-block");
		$('input[type=text],input[type=password],select').each(function(){
			var oldClass="";
			if($(this).attr('class')!="")
			{
				oldClass=$(this).attr('class');
				$(this).attr('class','form-control '+oldClass);
			}
			else
			{
				$(this).attr('class','form-control');
			}
		});
		//$('input[type=text],input[type=password]').attr("class","form-control");
		$("ul.dropdown-menu li.dropdown a.dropdown-toggle span.caret").addClass("right-caret");
		$("table.mmf_table thead th").addClass("header-th");
		$("ul.yiiPager").removeClass('yiiPager').addClass('pagination');
		
		$('input[readonly]').css({'cursor': 'not-allowed','background-color': 'rgb(238, 238, 238)','opacity':'1'});
		$('table.items').addClass("table table-bordered table-striped");
		$('table.mmf_table').addClass("table table-bordered table-striped");
	});
	function applyDefault()
	{
	
		
		$('.table > thead > tr > th').css({'padding' : '0px !important'});
		$('input[type=button]').attr("class","btn btn-lg btn-success btn-block");
		$('input[type=submit]').not('.login').attr("class","btn btn-lg btn-success btn-block");
		$('input[type=text],input[type=password]').each(function(){
			var oldClass="";
			if($(this).attr('class')!="")
			{
				oldClass=$(this).attr('class');
				$(this).attr('class','form-control '+oldClass);
			}
			else
			{
				$(this).attr('class','form-control');
			}
		});
		
		$("table.mmf_table thead th").addClass("header-th");
		$("ul.yiiPager").removeClass('yiiPager').addClass('pagination');
					$('table.mmf_table').addClass("table table-bordered table-striped");
		$('input[readonly]').css({'cursor': 'not-allowed','background-color': 'rgb(238, 238, 238)','opacity':'1'});

	}
	function applyAfterGridviewUpdate()
	{
		$('.grid-view .filters input[type=text]').attr("class","form-control");
		//$('.grid-view .filters select').attr("data-live-search","true");
		//$('.grid-view .filters select').selectpicker();	
		$("ul.yiiPager").removeClass('yiiPager').addClass('pagination');
	}
	
    $(document).ready(function() {
        $('#example').dataTable({
        	"pageLength": 10,
        //	"searching": false,
        	"lengthChange": false,
        	"scrollY": "250px",
        	"scrollX": true,
        	dom: 'Bfrtip',
            buttons: [
          
                {
                	
                    extend: 'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    className: 'btn btn-success',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    pageSize: 'A3',
                    exportOptions: {
		                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
		            }

                },
              {
              	extend: 'print',
              	text:      '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                 className: 'btn btn-success',
                exportOptions: {
		                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
		            }
                	
              }
                
            ]
        	//"scrollCollapse": true,
       
		        // "fixedColumns":   {
		        //     "leftColumns": 2
		        // }
        });
    });
   
//     $(document).ready(function() {
//     var table = $('#dataTables-example').DataTable( {
//     	pageLength: 30,
//         searching: false,
//         lengthChange: false,
//         scrollY:        "300px",
//         scrollX:        true,
//         scrollCollapse: true,
//         paging:         false,
//         fixedColumns:   {
//             leftColumns: 2
//         }
//     } );
// } );
	/*$("a.delete").click(function(){
		var r = confirm("Do you really want to delete this record?");
		if (r == true) {
		  return true;
		} else {
		 return false;
		}
	});*/
	</script>
	

<style>
.header-th
{
padding: 5px !important;
text-align: center;
border: 1px solid rgb(221, 221, 221);
background-color: rgb(243, 244, 245);
}

.mmf_table
{
	width:100%;
	margin-top:15px;
}
.mmf_cell
{
	border:none;
}
</style>
</html>

