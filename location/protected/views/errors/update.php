<?php 
/* @var $this ErrorsController */
/* @var $model Errors */

$this->breadcrumbs=array(
	'Errors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Errors', 'url'=>array('index')),
	array('label'=>'Create Errors', 'url'=>array('create')),
	array('label'=>'View Errors', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Errors', 'url'=>array('admin')),
);
?>

<h1>Update Errors <?php  echo $model->id; ?></h1>

<?php  $this->renderPartial('_form', array('model'=>$model)); ?>