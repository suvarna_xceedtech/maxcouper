<?php 
/* @var $this ErrorsController */
/* @var $model Errors */

$this->breadcrumbs=array(
	'Errors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Errors', 'url'=>array('index')),
	array('label'=>'Create Errors', 'url'=>array('create')),
	array('label'=>'Update Errors', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Errors', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Errors', 'url'=>array('admin')),
);
?>

<h1>View Errors #<?php  echo $model->id; ?></h1>

<?php  $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'is_gps_enabled',
		'created_at',
	),
)); ?>
