<?php 
/* @var $this ErrorsController */
/* @var $data Errors */
?>

<div class="view">

	<b><?php  echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php  echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php  echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('is_gps_enabled')); ?>:</b>
	<?php  echo CHtml::encode($data->is_gps_enabled); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php  echo CHtml::encode($data->created_at); ?>
	<br />


</div>