<style>
    input[type="radio"] {
   
    margin-left: 10px;
</style>
<?php 
/* @var $this UserLocationController */
/* @var $model UserLocation */

?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
<style type="text/css">
    
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDQffT82WtWIDw_P59J5Wp8XZ4s2otQP8&sensor=false"></script> 

<script type="text/javascript" src="js/map-font-icons.js"></script> 
 
 <?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'movie-master-form',
  // Please note: When you enable ajax validation, make sure the corresponding
  // controller action is handling ajax validation correctly.
  // There is a call to performAjaxValidation() commented in generated controller code.
  // See class documentation of CActiveForm for details on this.
  'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'), 

)); ?>
<div class="col-lg-12">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
           
            <div class="showDiv" >
                 <fieldset>
                <p class="note">Fields with <span class="required">*</span> are required.</p>
                <div class="row">
                <div class="ifUser">
                    <div class="control-group col-md-4">
                        
                        <div class="controls">
                        	<label>Select Driver</label>
                           <?php 
							$command=yii::app()->db->createCommand("SELECT `id`,driver_name name FROM `drivers` order by id");
							$user=$command->queryAll();
							$user_list = CHtml::listData($user, 'id', 'name');
                           $this->widget('ext.select2.ESelect2',array(
                              'name'=>'selectInput',
                              'data'=>$user_list,
                              'options'=>array(
                                    'placeholder'=>'-- Select Driver --',
                                    'allowClear'=>true,
                                 ),
                                'htmlOptions'=>array(
                                 'style'=>'width:100%;' ,
                                 'onchange'=>'getData(this)'      
                                ),
                            )); ?>
                        </div>
                    </div>
                </div>
                
             
            </div>
          </fieldset>
            </div>
           
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<?php $this->endWidget(); ?>
<div class="row mt-10" id="mapdata">
    <div id="map_canvas"></div>
</div>
<div class="row mt-10">
    <div id="resultTbl" style="width:100%;overflow-x:auto">
    
    </div>
</div>
<script type="text/javascript">

  function getData(ele)
  {
     
      var user_id=ele.value;

        var CoorPath="";
        var userCoor="";
        var coorArr="";
        var count=0;
        var result="";
        var pathArr=[];
        var coor="";
       
        $.ajax({
            type:'POST',
            data:{user_id:user_id },
            url:"<?php echo Controller::createUrl('API/getUsersLocations') ?>",
            success:function(items)
            {
                $( "#resultTbl" ).hide();
                var myarry = JSON.stringify(items);
              var stringify = JSON.parse(items);
              result= stringify;
              count= stringify.length;
              if(stringify.length>0)
              {
                userCoor = stringify;
                for(var index in stringify) {
                  coor=new google.maps.LatLng(stringify[0][1], stringify[0][2])
                  pathArr.push(new google.maps.LatLng(stringify[index][1], stringify[index][2])); 
                }
                var map;
                var mapOptions = { center: coor, zoom: 10,
                  mapTypeId: google.maps.MapTypeId.ROADMAP};
                map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
              var userCoorPath = pathArr;
              // var userCoordinate = new google.maps.Polyline({
              // path: userCoorPath,
              // strokeColor: "#FF0000",
              // strokeOpacity: 1,
              // strokeWeight: 2
              // });
              //userCoordinate.setMap(map);
                 var infowindow = new google.maps.InfoWindow({
            
                    disableAutoPan: false,
                    maxWidth: 300,
                    zIndex: null,
                    closeBoxMargin: "12px 4px 2px 2px",
                    infoBoxClearance: new google.maps.Size(1, 1)
                });
        
              var marker, i;
              for (i = 0; i < count; i++) {  
                marker = new google.maps.Marker({
                  position: new google.maps.LatLng(result[i][1], result[i][2]),
                  map: map,
                  icon: {
                            path: 'M-10,0a10,10 0 1,0 20,0a10,10 0 1,0 -20,0',
                            fillColor: result[i][3],
                            fillOpacity: 1,
                            strokeColor: result[i][4],
                            strokeWeight: 10
                        },
                });
    
                google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                  return function() {
                    infowindow.setContent(userCoor[i][0]);
                    infowindow.open(map, marker);
                  }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
                  return function() {
                    
                    infowindow.close(map, marker);
                  }
                })(marker, i));
              }
              google.maps.event.addDomListener(window, 'load', initialize);
              }
              else
              {
                $("#map_canvas").html("No records found");
              }
    
            }
          });
    
   
   
    return false;
   
  }
 
</script>

