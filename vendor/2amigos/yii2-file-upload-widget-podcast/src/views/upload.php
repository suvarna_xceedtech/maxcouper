<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size"><?= Yii::t('fileupload', 'Processing') ?>...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td style="width:180px">
            <?php
            $char_dropdown=\backend\models\Starsign::find()->where(['status'=>1])->all();?>
           <select name="{%=file.size%}"  class="form-select required form-control audiostarsign" required="required" aria-required="true">
                <option value="">-- select starsign --</option>';

            <?php foreach($char_dropdown as $chartdropdown)
            {
                echo '<option value="'.$chartdropdown->title.'" >'.$chartdropdown->title.'</option>';
            }?>
            </select>
            
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span><?= Yii::t('fileupload', 'Start') ?></span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span><?= Yii::t('fileupload', 'Cancel') ?></span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>