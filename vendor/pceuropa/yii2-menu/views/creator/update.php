<?php
use yii\helpers\Html;

pceuropa\menu\MenuAsset::register($this);
$this->title = Yii::t('app', 'Update Menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Update Menu: <?= $model->name?></h3>
        </div>
    </div>
    <div class="box-body">


		<?php echo $this->render('_menu');?>

		<div class="row">
			<?= $this->render('_form');?>
		</div>
	</div>
</div>




<?php 
$this->registerJs("var menu = new MyMENU.Menu({
	config: {
		setMysql: true,
		getMysql: true
	}

});", 4);
?>
