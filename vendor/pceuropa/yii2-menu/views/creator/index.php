<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Menu');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
  	<div class="box-header with-border">
	    <div class="col-md-8">
	        <h3 class="box-title"> <i class="fa fa-list"></i> Menu List</h3>
	    </div>
	    <div class="col-md-4">
	        <div class="pull-right">
	           <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Add'), ['create'], ['class' => 'btn btn-success']);?>
	        </div>
	    </div>
   
  	</div>
  	<div class="box-body">
		<?php
		echo  GridView::widget([
			'dataProvider' => $dataProvider,
			//'filterModel' => $searchModel,
			'columns' => [
					['class' => 'yii\grid\SerialColumn'],
		            [
		               'attribute'=>'menu_id',
		               'contentOptions'=>['style'=>'width:30px;']
		            ],
		                'name',
		                'description',
		            [
                    'header'=>'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                       return Html::a('<i class="fa fa-pencil"></i>', ['creator/update?id='.$model['menu_id']]);
                      
                    },
                ],
				//['class' => 'yii\grid\ActionColumn',],
			],
		]); 

		?>
	</div>
</div>
