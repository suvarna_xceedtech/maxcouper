<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    
    <!-- /.login-logo -->
   <!--  <div id="om-lightbox-case-study-optin-bar" class="boston-clearfix boston-optin-bar om-clearfix"><span class="boston-bar boston-yellow-bar om-bar om-red-bar"></span><span class="boston-bar boston-black-bar om-bar om-blue-bar"></span><span class="boston-bar boston-yellow-bar om-bar om-red-bar"></span><span class="boston-bar boston-black-bar om-bar om-blue-bar"></span></div> -->
    <div class="wrap-login100">
       
    <div id="logo">
    <center><b><span style="display: contents;letter-spacing:4.5px;align-items: center;color:white;font-size: 24px;padding: 20px;"> Max Couper Collection</span></b></center>

                           </div>
                           <br><br>
   <!--  <div class="login-box-body"> -->
        <p class="login-box-msg" style="color:#fff;"><strong>LOG IN</strong></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' =>"Username *"]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => "Password *"]) ?>

        <div class="row">
            <!-- <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div> -->
            <!-- /.col -->
            <br><br>
            <div class="col-xs-12">
                <center><?= Html::submitButton('Login', ['class' => 'btn btn-primary  btn-flat','id'=>'btn-login', 'name' => 'login-button']) ?></center>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>
           
        <!-- <center><br> <?= Html::a(
                                    'I forgot my password',
                                    ['site/forgotpassword'],
                                    ['class' => ' ']
                                ) ?></center><br>
 -->
    </div><!-- </div> -->
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
 <?php if(\Yii::$app->session->hasFlash('loginError')) : ?>
        <div class="alert alert-danger alert-dismissible" style="margin-top: 5%;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <?php echo \Yii::$app->session->getFlash('loginError'); ?>
        </div>
       <?php endif; ?>
<style type="text/css">
    .login-form, .register-page {
    height: auto;
    background: #d2d6de;
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: url(http://localhost/quantum_rose.in/backend/web/images/bg.png) transparent;
    background-size: cover;
    z-index: -1;

}
#loginform-password,#loginform-username{

    font-size: 16px;
  /* // color: #fff;*/
    line-height: 1.2;
    display: block;
    width: 100%;
    height: 45px;
   /* background-color: #eeeeee;*/
    padding: 0 5px 0 38px;
    border-radius: 10px;
 }

#btn-login{
    border-radius: 10px;
    font-size: 16px;
}



.btn-primary {
    background-color: #fff!important;
    border-color: #1abc9c!important;
    color:#1abc9c !important;
    width:25%;
    border-radius: 10px;
}
 .btn-primary:hover {
    background-color:  #fff!important;
    border-color: #1abc9c!important;
    color:#1abc9c;
}
    .boston-optin-bar {
    margin-bottom: 20px;
}
.boston-red-bar {
    background-color: #b81f1f;
}
.boston-bar {
    float: left;
    width: 25%;
    height: 11px;
    display: block;
}
.boston-green-bar {
    background-color: #abb92e;
}
.boston-yellow-bar {
    background-color:  #1abc9c;
}
 .boston-black-bar {
    background-color: #191919;
}
a:hover, a:active, a:focus {
   opacity: 0.8;color: #1abc9c;
}
a{
    color:#1abc9c;
}

.wrap-login100 {
    width: 500px;
    border-radius: 10px;
    overflow: hidden;
    padding: 55px 55px 37px 55px;
    background: #1abc9c;
    height: 400px;
}
</style>