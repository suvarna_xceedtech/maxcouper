<style type="text/css">
    
    .nav .open > a, .nav .open > a:hover, .nav .open > a:focus {
   
    border-color: #1abc9c !important;
}
</style>

<?php

use yii\bootstrap4\NavBar;
use kartik\nav\NavX;



// Usage with bootstrap nav pills.
echo NavX::widget([
    'options'=>['class'=>'nav nav-pills'],
    'items' => [
       
         
         ['label' => 'Home', 'url' => yii\helpers\Url::to(['home-main/index']), 'linkOptions'=> ['class'=>'text-logo-color']],
        //['label' => 'Shows', 'url' => yii\helpers\Url::to(['shows-collection/index']), 'linkOptions'=> ['class'=>'text-logo-color']],
        [
        'label' => '' . Yii::t('app', 'Shows'),
        'url' => ['#'],
        'class' => 'text-logo-color',
        'active' => false,
        'linkOptions'=> ['class'=>'text-logo-color'],
        'items' => [
            
            
            [
                'label' => ' ' . Yii::t('app', 'Category'),
                'url' => ['/category/index'],
                
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
            [
                'label' => '' . Yii::t('app', 'Shows Collection'),
                'url' => ['shows-collection/index'],
                
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
           
        ],
    ],
        
         ['label' => 'Blog', 'url' => yii\helpers\Url::to(['#']), 'linkOptions'=> ['class'=>'text-logo-color']],
         
         ['label' => 'About', 'url' => yii\helpers\Url::to(['#']), 'linkOptions'=> ['class'=>'text-logo-color']],
         ['label' => 'Writings ', 'url' => yii\helpers\Url::to(['#']), 'linkOptions'=> ['class'=>'text-logo-color']],

           ['label' => 'Learning ', 'url' => yii\helpers\Url::to(['#']), 'linkOptions'=> ['class'=>'text-logo-color']],
            ['label' => 'Media ', 'url' => yii\helpers\Url::to(['#']), 'linkOptions'=> ['class'=>'text-logo-color']],
             ['label' => 'Contact ', 'url' => yii\helpers\Url::to(['contact/index']), 'linkOptions'=> ['class'=>'text-logo-color']],
             ['label' => 'Setting ', 'url' => yii\helpers\Url::to(['setting/index']), 'linkOptions'=> ['class'=>'text-logo-color']],

        
       
        
    ]
]);

