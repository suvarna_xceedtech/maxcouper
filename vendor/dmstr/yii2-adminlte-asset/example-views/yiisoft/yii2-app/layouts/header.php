<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */
?>
<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="" style="width:100%"><!--container-->
        <div class="navbar-header">
            <div id="logo">
             <strong style="display: contents;letter-spacing:4.5px"> Max Couper <br><span>Collection</span></strong>
            </div>
          <button type="button" class="navbar-toggle collapsed text-logo-color" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <div class="navbar-custom-menu">
                 <?= $this->render('//layouts/top-menu.php') ?>
            </div>
          
        </div>
        <!-- /.navbar-collapse -->
        <div class="navbar-custom-menu notifications">
          <ul class="nav navbar-nav">
            <?php $user=\backend\models\Users::findOne(Yii::$app->user->identity->id);

            ?>
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <?php if($user->profile_pic!="" && $user->profile_pic!=NULL){ ?>
                  <img src="<?php echo Yii::$app->params['server']?><?php echo $user->profile_pic?>" class="user-image" alt="User Image">
               <?php }
                else
                  {?>
                    <img src=<?php echo Yii::$app->params['server']?>/images/user.jpg class="user-image" alt="User Image">
                 <?php }?>
                
                <span class="hidden-xs hidden-lg"><?= \Yii::$app->user->identity->username?></span>
              </a>
              <ul class="dropdown-menu dropdown-user animated">
                <div class="scroll-wrapper dropdown-user-scroll scrollbar-outer" ><div class="dropdown-user-scroll scrollbar-outer scroll-content" >
                  <li>
                    <div class="user-box">
                      <div class="avatar-lg">
                         <?php if($user->profile_pic!="" && $user->profile_pic!=NULL){ 
                            echo '<img src="'.Yii::$app->params['server'].$user->profile_pic.'" class="avatar-img rounded" alt="User Image" style="max-width: 45px;">';
                          }
                          else
                            {
                              echo '<img src="http://shelleyvonstrunckel.xceedtech.in/backend/web/images/user.jpg" alt="image profile" class="avatar-img rounded" style="max-width: 45px;">';
                            }?>
                        </div>
                      <div class="u-text">

                        <h4><?= Yii::$app->user->identity->name?></h4>
                        <p class="text-muted"><?= Yii::$app->user->identity->username?></p><a href="<?= \yii\helpers\Url::to(['site/profile','id'=>Yii::$app->user->identity->id])?>" class="btn btn-xs btn-primary btn-sm">View Profile</a>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="dropdown-divider"></div>
                    <?= Html::a(
                          'My Profile',
                          ['/site/profile', 'id'=>Yii::$app->user->identity->id],
                          ['data-method' => 'post', 'class' => 'dropdown-item']
                      ) ?>
                    <?= Html::a(
                          'Change Password',
                          ['/site/changepassword','id'=>Yii::$app->user->identity->id],
                          ['data-method' => 'post', 'class' => 'dropdown-item']
                      ) ?>
                    <div class="dropdown-divider"></div>
                    <?= Html::a(
                          'Logout',
                          ['/site/logout'],
                          ['data-method' => 'post', 'class' => 'dropdown-item']
                      ) ?>
                   
                  </li>
                </div>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

