<?php

/**
 * @author Prakash S
 * @copyright 2017
 */
 
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\db\Query;

class ModulesPermission extends Component
{
    public function getMenus()
    {
        $role_id = Yii::$app->user->identity->role_id;

        $user_id = Yii::$app->user->identity->id;
        $modules = \backend\models\ModulesList::find()
            ->select('module_id, module_name, controller, icon, url,parent_id')
            ->where('is_active = :is_active', [':is_active' => 1])
            ->andWhere('show_in_sidebar = :show_in_sidebar', [':show_in_sidebar' => 1])
            ->andWhere('parent_id = :parent_id', [':parent_id' => 0])
            ->asArray()->all();
        $items =""; 
        $items.='<ul class="sidebar-menu tree" data-widget="tree">';
        // $items.= '<li class="header">Menu</li>';
        $items.= '<li id="dashboard"><a href="'. \yii\helpers\Url::to(['site/']).'"><span class="fa fa-dashboard"></span> Dashboard</a></li>';
        for($i=0; $i<count($modules); $i++)
        {
            //echo "module:".$modules[$i]['module_name']."</br >";
            $submodules = \backend\models\UserModulePermission::find()
                     ->select('ML.module_id, ML.module_name, ML.controller, ML.icon, ML.url, ML.parent_id, user_module_permission.access')
                     ->join('LEFT JOIN', 'modules_list AS ML', 'ML.module_id = user_module_permission.module_id')
                     ->where('user_id = :user_id', [':user_id' => $user_id])
                     ->andWhere('user_module_permission.access = :access', [':access' => 1])
                     ->andWhere('is_active = :is_active', [':is_active' => 1])
                    ->andWhere('ML.show_in_sidebar = :show_in_sidebar', [':show_in_sidebar' => 1])
                     ->andWhere('ML.parent_id = :parent_id', [':parent_id' =>$modules[$i]['module_id']])
                     ->asArray()->all();
            if($role_id==1)
            {
                $submodules = \backend\models\ModulesList::find()
                    ->select('module_id, module_name, controller, icon, url, parent_id')
                    ->where('is_active = :is_active', [':is_active' => 1])
                    ->andWhere('show_in_sidebar = :show_in_sidebar', [':show_in_sidebar' => 1])
                    ->andWhere('parent_id = :parent_id', [':parent_id' =>$modules[$i]['module_id']])
                    ->asArray()->all();
            }
           
            if(!empty($submodules))
            {//echo "cd";die;
                
                $items.='<li class="treeview" id="'.$modules[$i]['controller'].'" ><a href="#"><i class="fa '.$modules[$i]['icon'].'"></i>  <span>'.$modules[$i]['module_name'].'</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">';
                        
                for($j=0; $j<count($submodules); $j++)
                {
                    
                    $items.= '<li id="'.$submodules[$j]['controller'].'" ><a href="'. \yii\helpers\Url::to([$submodules[$j]['url'].'/']).'"><span class="fa fa-angle-double-right"></span> '.$submodules[$j]['module_name'].'</a></li>';
                }
                $items.= '</ul></li>';
            }
            else
            {
                $CheckAccess = \backend\models\UserModulePermission::find()
                     ->select('access')
                     ->where('user_id = :user_id', [':user_id' => $user_id])
                     ->andWhere('module_id = :module_id', [':module_id' =>$modules[$i]['module_id']])
                     ->one();
                     //echo "CheckAccess:".$CheckAccess['access'];
                if($CheckAccess['access']==1)
                {
                    $items.= '<li id="'.$modules[$i]['controller'].'" ><a href="'. \yii\helpers\Url::to([$modules[$i]['url'].'/']).'"><span class="fa '.$modules[$i]['icon'].'"></span> '.$modules[$i]['module_name'].'</a></li>';
                }  
            }
        }
        $items.="</ul>";
        return $items;
    }
    
    public function getPermission()
    {
        $actions = array();
        $role_id = Yii::$app->user->identity->role_id;
        $user_id = Yii::$app->user->identity->id;      
        $action = Yii::$app->controller->action->id;
        $controller = Yii::$app->controller->id;
        if($role_id==1)
        {
            return true;

        }
        else
        {
            $permission = \backend\models\UserModulePermission::find()
                             ->select('ML.module_id, ML.module_name, ML.controller, ML.icon, user_module_permission.access')
                             ->join('LEFT JOIN', 'modules_list AS ML', 'ML.module_id = user_module_permission.module_id')
                             ->where('ML.action = :action', [':action' => $action])
                            ->andWhere('user_id = :user_id', [':user_id' => $user_id])
                             ->andWhere('user_module_permission.access = :access', [':access' => 1])
                             ->andWhere('controller = :controller', [':controller' => $controller])
                             ->one();
       // echo $permission["access"];die;
        return ($permission["access"]==1)? true : false;
        }
        
    }
    public function getAccessPermission($controller,$action)
    {
        $actions = array();
        $user_id = Yii::$app->user->identity->id;    
        $role_id = Yii::$app->user->identity->role_id;       
        if($role_id==1)
        {
            return true;
        }
        else
        {
            $permission = \backend\models\UserModulePermission::find()
                             ->select('ML.module_id, ML.module_name, ML.controller, ML.icon, user_module_permission.access')
                             ->join('LEFT JOIN', 'modules_list AS ML', 'ML.module_id = user_module_permission.module_id')
                             ->where('ML.action = :action', [':action' => $action])
                            ->andWhere('user_id = :user_id', [':user_id' => $user_id])
                             ->andWhere('user_module_permission.access = :access', [':access' => 1])
                             ->andWhere('controller = :controller', [':controller' => $controller])
                             ->one();
                             // echo $controller."->".$action;
            //echo $permission["access"];die;
            return ($permission["access"]==1)? true : false;
        }
       
        
    }
}

?>