<?php

namespace backend\controllers;

use Yii;
use backend\models\ShowsCollections;
use backend\models\ShowsCollectionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ShowsCollectionsDetails;
/**
 * ShowsCollectionController implements the CRUD actions for ShowsCollections model.
 */
class ShowsCollectionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShowsCollections models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShowsCollectionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShowsCollections model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShowsCollections model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate1()
    {
        $model = new ShowsCollections();

        if (Yii::$app->request->post() ) {
                
               // print_r($_POST);die;
         // print_r($_FILES['image1']);die;
            //first insert into shows collection table
            $model->category_id=$_POST['ShowsCollections']['category_id'];
            $model->title=$_POST['ShowsCollections']['title'];
            $model->sub_title=$_POST['ShowsCollections']['sub_title'];
             $model->sequence=$_POST['ShowsCollections']['sequence'];
                $model->created_by=Yii::$app->user->id;
                $model->created_at=new \yii\db\Expression('NOW()');

             $successdata=Yii::$app->db->createCommand()
                              ->insert('shows_collections', [
                             'category_id' => $model->category_id,
                             'title' => $model->title,
                             'sequence'=>$model->sequence,
                             'sub_title'=>$model->sub_title,
                              'created_by' =>  $model->created_by,
                              'created_at'=> $model->created_at,
                              'status'=>1,
                          ])->execute();

                     if($successdata==1)
                $collection_id = Yii::$app->db->getLastInsertID();

               //end show collection tale
           
                
                //insert  into shows_collection_details
            $num=$_POST['rowcount'];
            
            //echo $num;die;
            //count($_POST);die;
            for($i=1;$i<=$num;$i++) 
            {
            if($_POST['image_alt1']!="") {
            $created_by=Yii::$app->user->id;
            $created_at=new \yii\db\Expression('NOW()');
 

              //$title=$_POST['title'.$i];
              $image_alt=$_POST['image_alt'.$i];
              $sort=$_POST['sort'.$i];
              $leftside_description=$_POST['left_description'.$i];
              $bottom_description=$_POST['bottom_description'.$i];
                $image= $_FILES['image'.$i];
                $file_tmp=$_FILES['image'.$i]['tmp_name'];
                $file_name = $_FILES['image'.$i]['name'];
               move_uploaded_file($file_tmp,"images/Shows/".$file_name);
              $image='images/Shows/'. $file_name; 
             
             $successdata=Yii::$app->db->createCommand()
                              ->insert('shows_collections_details', [
                                'collection_id'=>$collection_id,
                             'sort' => $sort,
                             'image_alt'=> $image_alt,
                             'leftside_description' => $leftside_description,
                             'bottom_description'=>$bottom_description,
                             'image'=>$image,
                              'created_by' =>  $created_by,
                              'created_at'=> $created_at,
                              'status'=>1,
                          ])->execute();
         }
     }
         Yii::$app->session->setFlash('success', "Records Added successfully."); 
         return $this->redirect(['index']);


              // if($model->save())
              // {
              //    Yii::$app->session->setFlash('success', "Shows Collections Records Are Added Successfully."); 
              //   return $this->redirect(['index']);

              // }

            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShowsCollections model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate1($id)
    {
        $model = $this->findModel($id);
       //print_r($model);die;

        if (Yii::$app->request->post() )
         {

          //echo "<pre>";print_r($_POST);die;
           // $collection_id=$_POST['ShowsCollections']['id'];

         //to update Shows Collection

           $model->category_id=$_POST['ShowsCollections']['category_id'];
            $model->title=$_POST['ShowsCollections']['title'];
            $model->sub_title=$_POST['ShowsCollections']['sub_title'];
             $model->sequence=$_POST['ShowsCollections']['sequence'];
                $updated_by = Yii::$app->user->id;
            $updated_at = new \yii\db\Expression('NOW()');


              $successfully=Yii::$app->db->createCommand()->update('shows_collections', [
                               // 'title' => $title,
                             'category_id'=>$model->category_id,
                             'title' =>$model->title,
                             'sequence'=>$model->sequence,
                             'sub_title'=>$model->sub_title,
                              'updated_by' =>  $updated_by,
                              'updated_at'=> $updated_at,
                              ],'id = '. $id)->execute();

               ///To add in show_collection_details table
            
              $num=$_POST['rowcount'];
               //  echo $num;die;
              //echo "<pre>";print_r($_POST);die;
             // echo count($_POST);die;
            for($i=0;$i<=$num;$i++)
            {

                 //$title=$_POST['title'.$i];
             $leftside_description=$_POST['leftside_description'.$i];
              $bottom_description=$_POST['bottom_description'.$i];
              $image_alt=$_POST['image_alt'.$i];
               $sort=$_POST['sort'.$i];
               $updated_by = Yii::$app->user->id;
            $updated_at = new \yii\db\Expression('NOW()');

               
                   $idt=$_POST['id'.$i];

                   if(empty($idt))
                   {
                    $idt=0;
                   }

            $query=(new \yii\db\Query())->select('*')->from('shows_collections_details')->where('id='.$idt.' and collection_id='.$id.'');
                      $command = $query->createCommand();
                      $shows = $command->queryAll();
                      //print_r($shows[0]['image']);die;

             if(!empty($_FILES['image'.$i]['name']))
              {
                $image= $_FILES['image'.$i];
                $file_tmp=$_FILES['image'.$i]['tmp_name'];
                $file_name = $_FILES['image'.$i]['name'];
               move_uploaded_file($file_tmp,"images/Shows/".$file_name);
                $image='images/Shows/'. $file_name; 
              }else{
                $image=$shows[0]['image'];
              }


              if(!empty($shows))
              {                         
                 
                  $successfully=Yii::$app->db->createCommand()->update('shows_collections_details', [
                                'sort' => $sort,
                             'image_alt'=>$image_alt,
                             'leftside_description' =>$leftside_description,
                             'bottom_description'=>$bottom_description,
                             'image'=>$image,
                              'updated_by' =>  $updated_by,
                              'updated_at'=> $updated_at,
                              'status'=>1,
                    ],'id = '. $idt)->execute();
                  //echo $idt;die;
               }else{
                        //echo" hii";die;
               // echo $i;die;
                //echo $title,$image_alt;die;
                        $created_by=Yii::$app->user->id;
                        $created_at=new \yii\db\Expression('NOW()');

                        $successdata=Yii::$app->db->createCommand()
                              ->insert('shows_collections_details', [
                                'collection_id'=>$id,
                             'sort' => $sort,
                             'image_alt'=> $image_alt,
                             'leftside_description' => $leftside_description,
                             'bottom_description'=>$bottom_description,
                             'image'=>$image,
                              'created_by' =>  $created_by,
                              'created_at'=> $created_at,
                              'status'=>1,])->execute();

                   }
                                      
         }
         Yii::$app->session->setFlash('success', "Records Updates successfully."); 
         return $this->redirect(['index']);

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShowsCollections model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        //echo $id ;die;


        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     public function actionInactive($id)
    {

        $successfully=Yii::$app->db->createCommand()->update('shows_collections_details', [
                                                              'status'=>0,
                    ],'id = '. $id)->execute();
            if($successfully==1)
         {
             Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
        }
    }




    /**
     * Finds the ShowsCollections model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShowsCollections the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShowsCollections::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     public function actionMultipledelete()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            
            
            //$this->findModel($value)->delete();
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
    }


    public function actionCreate()
    {
        $model = new ShowsCollections();


        if ($model->load(Yii::$app->request->post()) ) 
        {
            
              $model->created_by=Yii::$app->user->id;
             $model->created_at = new \yii\db\Expression('NOW()');
            

            if($model->save(false))
            {
                 Yii::$app->session->setFlash('success', "Shows Collection  Are Added Successfully."); 
                 return $this->redirect(['index']);
            }
           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

      public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {

            if( $model->save()){
                Yii::$app->session->setFlash('success', "Shows Collection Records Are Updated Successfully."); 

                 return $this->redirect(['index']);

            }
           
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


  public function actionSaveContinue()
  {
     $model = new ShowsCollections();


        if ($model->load(Yii::$app->request->post()) ) 
        {
           
            $model->created_by=Yii::$app->user->id;
             $model->created_at = new \yii\db\Expression('NOW()');
            

           // print_r($_POST);die();

            if($model->save(false))
            {

         //print_r($model);die;
            Yii::$app->session->setFlash('success', "Shows Collection Records Are Save Successfully."); 
                return $this->redirect(['update','id'=>$model->id]);
            }
        }

         return $this->render('create', [
            'model' => $model,
        ]);

  }

}
