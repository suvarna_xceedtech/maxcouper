<?php

namespace backend\controllers;

use Yii;
use backend\models\HomeMain;
use backend\models\HomeMainSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HomeMainController implements the CRUD actions for HomeMain model.
 */
class HomeMainController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomeMain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomeMainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomeMain model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomeMain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HomeMain();

        if ($model->load(Yii::$app->request->post()) ) 
        {
            $model->home_image1 = UploadedFile::getInstance($model, 'home_image1');
             $model->home_image2 = UploadedFile::getInstance($model, 'home_image2');
            $model->created_by=Yii::$app->user->id;
             $model->created_at = new \yii\db\Expression('NOW()');
            if (isset($model->home_image1))
             { 

                 $model->home_image1->saveAs('images/Home/' . $model->home_image1->baseName . '.' . $model->home_image1->extension, false);
                 $model->home_image1='images/Home/'.$model->home_image1;
                

            }

            if (isset($model->home_image2))
             { 

                 $model->home_image2->saveAs('images/Home/' . $model->home_image2->baseName . '.' . $model->home_image2->extension, false);
                 $model->home_image2='images/Home/'.$model->home_image2;
                

            }

            if($model->save())
            {
                 Yii::$app->session->setFlash('success', "Home Records Are Added Successfully."); 
                 return $this->redirect(['index']);
            }
           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HomeMain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
          $img1=$model->home_image1;
          $img2=$model->home_image2;
           //print_r($model->image);die;
         if ($model->load(Yii::$app->request->post()))
         {
                $model->home_image1 = UploadedFile::getInstance($model, 'home_image1');
                $model->home_image2 = UploadedFile::getInstance($model, 'home_image2');
             $model->updated_by = Yii::$app->user->id;
            $model->updated_at = new \yii\db\Expression('NOW()');
         
            
            //print_r($model->image);die;
            if($model->home_image1=='')
            {
                $model->home_image1=$img1;
            }
            if ($model->home_image1!=$img1)
             { 

                 $model->home_image1->saveAs('images/Home/' . $model->home_image1->baseName . '.' . $model->home_image1->extension, false);
                 $model->home_image1='images/Home/'.$model->home_image1;
                

            }

           if($model->home_image2=='')
            {
                $model->home_image2=$img2;
            }
            if ($model->home_image2!=$img2)
             { 

                 $model->home_image2->saveAs('images/Home/' . $model->home_image2->baseName . '.' . $model->home_image2->extension, false);
                 $model->home_image2='images/Home/'.$model->home_image2;
                

            }


            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Home Records Are Updated Successfully."); 
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HomeMain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomeMain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HomeMain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeMain::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



 

 public function actionSaveContinue()
  {
     $model = new HomeMain();


        if ($model->load(Yii::$app->request->post()) ) 
        {
            $model->home_image1 = UploadedFile::getInstance($model, 'home_image1');
             $model->home_image2 = UploadedFile::getInstance($model, 'home_image2');
            $model->created_by=Yii::$app->user->id;
             $model->created_at = new \yii\db\Expression('NOW()');
            if (isset($model->home_image1))
             { 

                 $model->home_image1->saveAs('images/Home/' . $model->home_image1->baseName . '.' . $model->home_image1->extension, false);
                 $model->home_image1='images/Home/'.$model->home_image1;
                

            }

            if (isset($model->home_image2))
             { 

                 $model->home_image2->saveAs('images/Home/' . $model->home_image2->baseName . '.' . $model->home_image2->extension, false);
                 $model->home_image2='images/Home/'.$model->home_image2;
                

            }

           // print_r($_POST);die();

            if($model->save())
            {

         //print_r($model);die;
            Yii::$app->session->setFlash('success', "Home Records Are Save Successfully."); 
                return $this->redirect(['update','id'=>$model->id]);
            }
        }

         return $this->render('create', [
            'model' => $model,
        ]);

  }



 











    public function actionMultipledelete()
    {
         $model = new HomeMain();

        $pk = Yii::$app->request->post('row_id');

        //echo"$pk ";die;
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
    }
}
