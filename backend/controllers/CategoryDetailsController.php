<?php

namespace backend\controllers;

use Yii;
use backend\models\CategoryDetails;
use backend\models\CategoryDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * CategoryDetailsController implements the CRUD actions for CategoryDetails model.
 */
class CategoryDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CategoryDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoryDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategoryDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategoryDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoryDetails();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CategoryDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $category_id=$model->category_id;
        //echo $home_main_id;die;
        // $img=$model->image;
        $img1=$model->image;
        if ($model->load(Yii::$app->request->post())) 
        {

              $model->image = UploadedFile::getInstance($model, 'image');
              $model->short_text=$_POST['CategoryDetails']['short_text'];
             $model->detail_text=$_POST['CategoryDetails']['detail_text'];
              $model->title=$_POST['CategoryDetails']['title'];
             
             $model->updated_by = Yii::$app->user->id;
             $model->updated_at = new \yii\db\Expression('NOW()');


              if($model->image=='')
            {
                $model->image=$img1;
            }
            if ($model->image!=$img1)
             { 

                 // $model->image->saveAs('images/Category/' . $model->image->baseName . '.' . $model->image->extension, false);
                $model->image->saveAs('images/Category/' . $model->image, false);
                 $model->image='images/Category/'.$model->image;
                

            }
         
            
            //print_r($model->image);die;
            

            if( $model->save())
            {
                 Yii::$app->session->setFlash('success', " Category Details : ".$model->title."  Are Updated Successfully."); 
                 return $this->redirect(['category/update', 'id' => $category_id]);
            }
          
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CategoryDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategoryDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CategoryDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CategoryDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



      public function actionAddCategoryDetails($id)
 {
   //Home Main id : $id
    //echo $id;die;

    $model = new CategoryDetails();

          $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()))
        {
             
             $model->category_id=$id;
              $model->title=$_POST['CategoryDetails']['title'];
             $model->short_text=$_POST['CategoryDetails']['short_text'];
             $model->detail_text=$_POST['CategoryDetails']['detail_text'];
              $model->image = UploadedFile::getInstance($model, 'image');
         

          if (isset($model->image))
             { 

                 $model->image->saveAs('images/Category/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Category/'.$model->image;
                

            }
          
           if($model->save(false))
           {
             Yii::$app->session->setFlash('success', "Category Details Are Added Successfully.");

             return $this->redirect(['category/update', 'id' => $id]);
           }

           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
 }



 public function actionInactive($id)
    {
        $model=$this->findModel($id);
          
          //print_r($model);die;
        $model->status=0;

       if($model->save()){



       Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['category/update','id'=>$model->category_id]);
        }
    }
}
