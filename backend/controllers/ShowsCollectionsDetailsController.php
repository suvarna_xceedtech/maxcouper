<?php

namespace backend\controllers;

use Yii;
use backend\models\ShowsCollectionsDetails;
use backend\models\ShowsCollectionsDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ShowsCollectionsDetailsController implements the CRUD actions for ShowsCollectionsDetails model.
 */
class ShowsCollectionsDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShowsCollectionsDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShowsCollectionsDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShowsCollectionsDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShowsCollectionsDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShowsCollectionsDetails();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShowsCollectionsDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $collection_id=$model->collection_id;
        //echo $home_main_id;die;
        $img=$model->image;

        if ($model->load(Yii::$app->request->post())) 
        {
             $model->image = UploadedFile::getInstance($model, 'image');
             $model->updated_by = Yii::$app->user->id;
             $model->updated_at = new \yii\db\Expression('NOW()');

             $model->title=$_POST['ShowsCollectionsDetails']['title'];
             $model->leftside_description=$_POST['ShowsCollectionsDetails']['leftside_description'];
             $model->bottom_description=$_POST['ShowsCollectionsDetails']['bottom_description'];
              $model->image_alt=$_POST['ShowsCollectionsDetails']['image_alt'];
               $model->sort=$_POST['ShowsCollectionsDetails']['sort'];
         
            
            //print_r($model->image);die;
            if($model->image=='')
            {
                $model->image=$img;
            }
            if ($model->image!=$img)
             { 

                 $model->image->saveAs('images/Shows/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Shows/'.$model->image;
                

            }

            if($model->save())
            {
                 Yii::$app->session->setFlash('success', " Shows Collection : ".$model->title."  Are Updated Successfully."); 
                 return $this->redirect(['shows-collection/update', 'id' => $collection_id]);
            }
          
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShowsCollectionsDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShowsCollectionsDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShowsCollectionsDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShowsCollectionsDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



     public function actionAddCollectionsDetails($id)
 {
   //Home Main id : $id
    //echo $id;die;

    $model = new ShowsCollectionsDetails();

          $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()))
        {
             
             $model->collection_id=$id;
              $model->title=$_POST['ShowsCollectionsDetails']['title'];
             $model->leftside_description=$_POST['ShowsCollectionsDetails']['leftside_description'];
             $model->bottom_description=$_POST['ShowsCollectionsDetails']['bottom_description'];
              $model->image_alt=$_POST['ShowsCollectionsDetails']['image_alt'];
               $model->sort=$_POST['ShowsCollectionsDetails']['sort'];
              $model->image = UploadedFile::getInstance($model, 'image');
         

          if (isset($model->image))
             { 

                 $model->image->saveAs('images/Shows/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Shows/'.$model->image;
                

            }
          
           if($model->save())
           {
             Yii::$app->session->setFlash('success', "Shows Collection Details Are Added Successfully.");

             return $this->redirect(['shows-collection/update', 'id' => $id]);
           }

           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
 }


 public function actionInactive($id)
    {
        $model=$this->findModel($id);
        $model->status=0;

       if($model->save(false))
       {
       Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['shows-collection/update','id'=>$model->collection_id]);
        }
        
    }

}
