<?php

namespace backend\controllers;

use Yii;
use backend\models\Setting;
use backend\models\SettingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Setting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Setting model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Setting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Setting();
           $model->setScenario('create');

        if ($model->load(Yii::$app->request->post())) {
            $model->robots = UploadedFile::getInstance($model, 'robots');
             $model->sitemap = UploadedFile::getInstance($model, 'sitemap');
            $model->created_by=Yii::$app->user->id;
           // echo(Yii::getAlias('@root'));die;
            $path=Yii::getAlias('@root').'/';
            if (isset($model->sitemap))
             { 

                 $model->sitemap->saveAs($path . $model->sitemap->baseName . '.' . $model->sitemap->extension, false);
                 $model->sitemap=$path.$model->sitemap;
                

            }

            if (isset($model->robots))
             { 

                 $model->robots->saveAs($path. $model->robots->baseName . '.' . $model->robots->extension, false);
                 $model->robots=$path.$model->robots;
                

            }

            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Setting Records Are Added Successfully."); 
            return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Setting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

         $img=$model->robots;
         $file1=$model->sitemap;
           //print_r($model->image);die;
         if ($model->load(Yii::$app->request->post()))
         {
             $path=Yii::getAlias('@root').'/';
                $model->robots = UploadedFile::getInstance($model, 'robots');
                $model->sitemap = UploadedFile::getInstance($model, 'sitemap');
             $model->updated_by = Yii::$app->user->id;
            $model->updated_at = new \yii\db\Expression('NOW()');
         
            
            //print_r($model->image);die;
            if($model->robots=='')
            {
                $model->robots=$img;
            }
            if ($model->robots!=$img)
             { 

                 $model->robots->saveAs($path . $model->robots->baseName . '.' . $model->robots->extension, false);
                 $model->robots=$path.$model->robots;
                

            }

//sitemap

             if($model->sitemap=='')
            {
                $model->sitemap=$file1;
            }
            if ($model->sitemap!=$file1)
             { 

                 $model->sitemap->saveAs($path . $model->sitemap->baseName . '.' . $model->sitemap->extension, false);
                 $model->sitemap=$path.$model->sitemap;
                

            }


            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Setting Records Are Updated Successfully."); 
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Setting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Setting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Setting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


     public function actionMultipledelete()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            
            
            //$this->findModel($value)->delete();
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
    }
}
