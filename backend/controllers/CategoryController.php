<?php

namespace backend\controllers;

use Yii;
use backend\models\Category;
use backend\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       $model = new Category();

        if ($model->load(Yii::$app->request->post()) ) 
        {
            
              $model->created_by=Yii::$app->user->id;
             $model->created_at = new \yii\db\Expression('NOW()');
            

            if($model->save(false))
            {
                 Yii::$app->session->setFlash('success', "Category Records Are Added Successfully."); 
                 return $this->redirect(['index']);
            }
           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {

            if( $model->save()){
                Yii::$app->session->setFlash('success', "Category Records Are Updated Successfully."); 

                 return $this->redirect(['index']);

            }
           
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSaveContinue()
  {
     $model = new Category();


        if ($model->load(Yii::$app->request->post()) ) 
        {
           
            $model->created_by=Yii::$app->user->id;
             $model->created_at = new \yii\db\Expression('NOW()');
            

           // print_r($_POST);die();

            if($model->save(false))
            {

         //print_r($model);die;
            Yii::$app->session->setFlash('success', "Category Records Are Save Successfully."); 
                return $this->redirect(['update','id'=>$model->id]);
            }
        }

         return $this->render('create', [
            'model' => $model,
        ]);

  }



     public function actionMultipledelete()
    {
         $model = new Category();

        $pk = Yii::$app->request->post('row_id');

        //echo"$pk ";die;
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
    }
}
