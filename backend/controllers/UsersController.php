<?php

namespace backend\controllers;

use Yii;
use backend\models\Users;
use app\models\User;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use fedemotta\datatables\DataTables;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $status="";
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$status);
        $model=new Users();
        $model->scenario = Users::SCENARIO_REPORT;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
        
    }

    public function actionAdd()
    {
        $model = new Users();
        $model->scenario = Users::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at=date('Y-m-d H:i:s');
          $model->username=$_POST['Users']['email'];
          $model->birth_date=date_format(date_create($_POST['Users']['birth_date']),'Y-m-d');
          $model->password=sha1($model->password);
          if($model->save())
          {
                Yii::$app->session->setFlash('success', "User created successfully"); 
                return $this->redirect(['index']);
          }
        }

        return $this->render('create', [
          'model' => $model,
        ]);
        
    }
    public function actionAddSubscription()
    {
        $model = new \backend\models\Cart();
        $model->scenario = \backend\models\Cart::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post())) {
           $status="";
           $model->status='Confirmed';
           $model->start_date=date_format(date_create($_POST['Cart']['start_date']),'Y-m-d');
           $model->end_date=date_format(date_create($_POST['Cart']['end_date']),'Y-m-d');
           if($model->save())
           {
            if($_POST['Cart']['status']=='Confirm')
                $status='succeeded';

                $user=Users::findOne($model->user_id);
                $name=$user->first_name;
                $email=$user->email;
                $subscription=\backend\models\Subscription::findOne($model->item_id);
                $amount=$subscription->amount;
                $payment = new \backend\models\TblPayment();
                $payment->email=$email;
                $payment->user_id=$model->user_id;
                $payment->currency_code='gbp';
                $payment->payment_status=$status;
                $payment->amount=$amount;
                $payment->create_at=date('Y-m-d H:i:s');
                $payment->txn_id='0';
                $payment->payment_response='-';
                if($payment->save())
                {
                    //print_r($payment->errors);die;
                    $orders = new \backend\models\Orders();
                    $orders->scenario = \backend\models\Orders::SCENARIO_CREATE;
                   
                    $orders->user_id=$model->user_id;

                    $orders->payment_id=$payment->id;

                    $orders->cart_id=$model->id;
                   
                    $orders->name=$name;
                    
                    $orders->email=$email;

                    $orders->payment_status=$status;

                    $orders->created_date=date('Y-m-d H:i:s');
                    $orders->amount=$amount;
                    if($orders->save())
                    {
                        Yii::$app->session->setFlash('success', "Subscription added for '".$name."' successfully"); 
                        return $this->redirect(['subscription']);

                    }
                    else
                    {
                       // print_r($orders->errors);die;
                    }

                }
                else
                {
                 // print_r($payment->errors);die;
                }
                
           }
           else
           {
            //print_r($model->errors);die;
           }
        }
       
        return $this->render('create_subscription', [
          'model' => $model,
        ]);
        
    }
    public function actionSubscription()
    {
       // $searchModel = new \backend\models\CartSearch();
        $query = \backend\models\Cart::find()
                     ->select('cart.id as id, ML.id as orderid, cart.item_id, cart.user_id, cart.session_id, cart.created_at, cart.start_date, cart.end_date, cart.ip_address, cart.starsign, cart.status, cart.is_chart')
                     ->join('LEFT JOIN', 'orders AS ML', 'ML.cart_id = cart.id')
                     ->where('cart.is_chart = :is_chart', [':is_chart' => 0])
                     ->andWhere('cart.status = :status', [':status' => 'Confirmed'])
                    ->andWhere('ML.payment_status = :payment_status', [':payment_status' => 'succeeded']);
                   // ->asArray()->all();
      //  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->render('subscription', [
           // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionGetenddate()
    {
        $date=$_REQUEST['date'];
      $duration=$_REQUEST['duration'];
        $fulldate = strtotime($date);
        $new_date = strtotime('+ '.$duration.' month', $fulldate);
        $end_date= date('d-m-Y', $new_date);
        return $end_date;


    }

    public function actionUpdateSubscription($id)
    {
        $model = \backend\models\Cart::findOne($id);

        //print_r($model);
        $model->scenario = \backend\models\Cart::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post())) {
           $status="";
           $model->status='Confirmed';
           $model->start_date=date_format(date_create($_POST['Cart']['start_date']),'Y-m-d');
           $model->end_date=date_format(date_create($_POST['Cart']['end_date']),'Y-m-d');
           if($model->save())
           {
           
                $user=Users::findOne($model->user_id);
                $name=$user->first_name;
                Yii::$app->session->setFlash('success', "Subscription of ".$name." updated successfully"); 
                return $this->redirect(['subscription']);    
           }
        }
       
        return $this->render('update_subscription', [
          'model' => $model,
        ]);
        
    }
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

   
    public function actionChangestatus($id,$flag)
    {
        $success=Yii::$app->db->createCommand()
                    ->update('user', [
                    'status'=>$flag
                    ],'id = '. $id)->execute();
 
        return $this->redirect(['index']);

    }
    public function actionUnblock($id,$flag)
    {
        $success=Yii::$app->db->createCommand()
                    ->update('user', [
                    'status'=>1
                    ],'id = '. $id)->execute();
        return $this->redirect(['index']);

    }

    public function actionGetcompanywiselisting( $status)
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $status);
        $data="";
        echo  DataTables::widget([
                  'dataProvider' => $dataProvider,
                  'id'=>'result',
                  'clientOptions' => [
                      'lengthMenu' => [200],
                      'responsive' => true, 
                      'dom' => 'BfTrtip',
                      'buttons' => [
                        'csv','excel', 'pdf', 'print'
                        ]
                    ],
                  'columns' => [
                     // ['class' => 'yii\grid\SerialColumn'],
                        
                         [
                            'attribute' => 'fullname',
                            'value' => function($data) { return $data->first_name  ." ". $data->last_name; }
                         ],
                        'username',
                        'starsign',
                        'email',
                        [
                            'attribute' => 'birth_date',
                            'value' => function ($model) { 
                                   return ($model->birth_date!="0000-00-00")?date_format(date_create($model->birth_date),'d-m-Y'):"";
                            },
                        ],
                        [
                            'header' => 'Last Visit Date',
                            'value' => function ($model) { 
                                   return $this->getLastVisit($model->id);
                            },
                        ], 
                        [
                            'attribute' => 'created_at',
                            'header' => 'Registration Date',
                            'value' => function ($model) { 
                                   return ($model->created_at!="0000-00-00")?date_format(date_create($model->created_at),'d-m-Y H:i:s'):"";
                            },
                        ], 
                        
                        [
                            'label'=> '#',
                            'format'=> 'raw',
                            'value' => function($model) { 
                                 return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['users/changestatus?id='.$model->id.'&flag=2'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this user?')):Html::a('<i class="fa fa-check"></i>', ['users/changestatus?id='.$model->id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this user?'));
                            }
                        ],
                    ],
              ]).'<script>jQuery(function ($) {
$("#datatables_result").DataTable();
});</script> ';

    }

    public function actionGetstatuswiselisting()
    {
        if($_POST)
        {
            $status=$_POST['status'];

            $data=$this->actionGetcompanywiselisting($status);
           // echo $data;
        }
    }

    
    public function showbtn($status, $id)
    {
        if($status==1)
            return Html::a('Inactive', ['users/changestatus?id='.$id.'&flag=2'],array('class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to block this user?'));
        else if($status==2)
            return Html::a('Active', ['users/changestatus?id='.$id.'&flag=1'],array('class'=>'btn btn-green','data-confirm'=>'Are you sure you want to activate this user?'));
        else
            return "";
    }

    public function getLastVisit($id)
    {
        $login = \app\models\LoginDetails::find()->where(['login_user_id' => $id])->orderBy(['login_detail_id'=>SORT_DESC])->one();
        return (!empty($login))?date_format(date_create($login->login_at),'d-m-Y  H:i:s'):"Never";
    }

}
