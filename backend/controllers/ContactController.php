<?php

namespace backend\controllers;

use Yii;
use backend\models\Contact;
use backend\models\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contact();

         $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()))
         {


            $model->image = UploadedFile::getInstance($model, 'image');
            $model->created_by=Yii::$app->user->id;
            if (isset($model->image))
             { 

                 $model->image->saveAs('images/Contact/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Contact/'.$model->image;
                

            }
            if($model->save())
            {// echo"hii";die;
                Yii::$app->session->setFlash('success', "Contacts Records Are Added Successfully."); 
                return $this->redirect(['index']);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $img=$model->image;
           //print_r($model->image);die;
         if ($model->load(Yii::$app->request->post()))
         {
                $model->image = UploadedFile::getInstance($model, 'image');
             $model->updated_by = Yii::$app->user->id;
            $model->updated_at = new \yii\db\Expression('NOW()');
         
            
            //print_r($model->image);die;
            if($model->image=='')
            {
                $model->image=$img;
            }
            if ($model->image!=$img)
             { 

                 $model->image->saveAs('images/Contact/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Contact/'.$model->image;
                

            }

            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Contact Records Are Updated Successfully."); 
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contact::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


     public function actionMultipledelete()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            
            
            //$this->findModel($value)->delete();
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
    }

}
