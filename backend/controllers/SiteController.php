<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use yii\web\UploadedFile;

/**
* Site controller
*/
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','forgotpassword'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'changepassword', 'profile', 'edit-profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                   
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $login = new \app\models\LoginDetails();

        if ($model->load(Yii::$app->request->post()) ) {
            $log = \app\models\User::find()->where(['username' => $_POST['LoginForm']['username'], 'status' => 1, 'user_type'=>1])->one();
           // print_r($_POST['LoginForm']['username']);die;
            if(empty($log)) {
                \Yii::$app->session->setFlash('loginError', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model]);
            }
            $login->login_user_id = $log['id'];
            $login->login_status = 1;
            $login->login_at = new \yii\db\Expression('NOW()');
            $login->user_ip_address=$_SERVER['REMOTE_ADDR'];
            $login->save(false);
            if($model->login()) {
                return $this->goBack();
            }
            else
                return $this->render('login', ['model' => $model,]);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {

        if(isset(Yii::$app->user->id))
        \app\models\LoginDetails::updateAll(['login_status' => 0, 'logout_at'=> new \yii\db\Expression('NOW()')],'login_user_id='.Yii::$app->user->id.' AND login_status = 1');
        Yii::$app->user->logout();
        return $this->redirect(['login']);

        //return $this->goHome();
    }
    public function actionPermission()
    {
          return $this->render('denied');
    }

    public function actionProfile($id)
    {
        $model = \app\models\User::findOne($id);

        return $this->render('profile',[
                    'model'=>$model
                ]);
    }

    public function actionEditProfile($id)
    {
        $model = \app\models\User::findOne($id);
        $img=$model->profile_pic;
        if ($model->load(Yii::$app->request->post()) ) {
            $model->birth_date=date_format(date_create($_POST['User']['birth_date']),'Y-m-d');

            $model->first_name=$_POST['User']['first_name'];
            $model->username=$_POST['User']['username'];
                $model->profile_pic = UploadedFile::getInstance($model, 'profile_pic');

            // if(isset($model->photo))
            // {
            //     $image=$model->photo;
            //     $uploaddir="profile";
            //     $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
            //     $filename=$model->id.".jpg";
            //     $image_name=time().$filename;
            //     $newname=$uploaddir."/".$image_name; 
            //     file_put_contents($newname, $data); 
            //     $filepath=Yii::$app->params['server'].''.$newname;
            //     $model->photo="";
            //     $model->profile_pic = $filepath; 
            //     //print_r($model->profile_pic);die;
            // }

             if($model->profile_pic=='')
            {
                $model->profile_pic=$img;
            }
            if ($model->profile_pic!=$img)
             { 

                 $model->profile_pic->saveAs('images/' . $model->profile_pic->baseName . '.' . $model->profile_pic->extension, false);
                 $model->profile_pic='images/'.$model->profile_pic;
                

            }
               //print_r($model->profile_pic);die;

            //print_r($_POST);die;
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Profile updated successfully."); 
                return $this->redirect(['profile', 'id'=>$id]);
            }
        }
        return $this->render('edit_profile',[
                    'model'=>$model
                ]);
    }


    public function actionChangepassword($id)
    {
        $model = \backend\models\Users::findOne($id);

        if ($model->load(Yii::$app->request->post()) )
        {
            //print_r($_POST);die;
             $oldpass=$_POST['Users']['oldpass'];
            $new_password=$_POST['Users']['newpass'];
             $password=$_POST['Users']['repeatnewpass'];

             if(sha1($oldpass)!==$model->password)
             {
              Yii::$app->session->setFlash('warning', "Old password does not match"); 
               return $this->redirect(['changepassword','id'=>$id]);
             }
          
          if($new_password!==$password)
          {
            Yii::$app->session->setFlash('warning', "New password and Confirm password must be same"); 
            return $this->redirect(['changepassword','id'=>$id]);
          }else{

            $model->password=sha1($password);

            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Password change successfully."); 
                return $this->redirect(['changepassword','id'=>$id]);

            }
          }
        }

        return $this->render('changepassword',[
                    'model'=>$model
                ]);
    }

   
}