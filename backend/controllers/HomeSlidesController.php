<?php

namespace backend\controllers;

use Yii;
use backend\models\HomeSlides;
use backend\models\HomeSlidesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * HomeSlidesController implements the CRUD actions for HomeSlides model.
 */
class HomeSlidesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomeSlides models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomeSlidesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomeSlides model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomeSlides model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HomeSlides();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HomeSlides model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $home_main_id=$model->home_main_id;
        //echo $home_main_id;die;
        $img=$model->image;

        if ($model->load(Yii::$app->request->post())) 
        {
             $model->image = UploadedFile::getInstance($model, 'image');
             $model->updated_by = Yii::$app->user->id;
             $model->updated_at = new \yii\db\Expression('NOW()');
         
            
            //print_r($model->image);die;
            if($model->image=='')
            {
                $model->image=$img;
            }
            if ($model->image!=$img)
             { 

                 $model->image->saveAs('images/Home/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Home/'.$model->image;
                

            }

            if($model->save())
            {
                 Yii::$app->session->setFlash('success', " Home Slide : ".$model->title."  Are Updated Successfully."); 
                 return $this->redirect(['home-main/update', 'id' => $home_main_id]);
            }
          
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HomeSlides model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomeSlides model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HomeSlides the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeSlides::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionAddHomeSlide($id)
 {
   //Home Main id : $id
    //echo $id;die;

    $model = new HomeSlides();

          $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()))
        {
             $model->image = UploadedFile::getInstance($model, 'image');
             $model->home_main_id=$id;
             
          if (isset($model->image))
             { 

                 $model->image->saveAs('images/Home/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image='images/Home/'.$model->image;
                

            }

           if($model->save(false))
           {
             Yii::$app->session->setFlash('success', "Home Slide Are Added Successfully.");

             return $this->redirect(['home-main/update', 'id' => $id]);
           }

           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
 }



 public function actionInactive($id)
    {
        $model=$this->findModel($id);
        $model->status=0;
       if($model->save()){
       Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['home-main/update','id'=>$model->home_main_id]);
        }
        
    }

 
}
