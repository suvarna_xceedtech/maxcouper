<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;
use backend\models\CategoryDetails;


$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
/* @var $this yii\web\View */
/* @var $model backend\models\Podcast */
/* @var $form yii\widgets\ActiveForm */
$category=CategoryDetails::find()->where(['status'=>1,'category_id'=>$model->id])->all();
    // print_r($home);die;

//print_r($model);die;
?>

<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>

         
<div class="podcast-form">
    <div class="card">
        <div class="card-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
              <div class="box-header with-border">
                <div class="col-md-8">
                    <h3 class="box-title"> <i class="fa fa-pencil"></i> Update Category Details</h3>
                </div>
                <div class="col-md-4">
                    <div class="pull-right">
                        <?= Html::submitButton('Save', ['id'=>'submit_upper','class' => 'btn btn-success']) ?>
                       
                    </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                         <div class="form-group ">
                         <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
                       </div>
                    </div>
                     <div class="col-md-12">
                         <div class="form-group ">
                         <?= $form->field($model, 'sequence')->textInput(['maxlength' => true])?>
                       </div>
                    </div>
                </div>

         <!-- // add slide -->

        <div class="row">
          
         <?php foreach($category as $homedata)
         { ?>
          <div class="col-md-3">  
                        <div class="form-group">
                            <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required"><?php echo $homedata['title'];?><span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                                  <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                                  <!-- <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required"><?php echo $homedata['title'];?></label> -->
                                    <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                        <div class="form-managed-file__main">
                                            <img src="<?= Yii::$app->params['server']; echo $homedata['image'];?>" style="width:200px;height: 100px;">
                                          
                                        </div>

                                    </div>
                                     <center><?=  Html::a('<i class="fa fa-pencil"></i>', ['category-details/update?id='.$homedata['id']]);?>&nbsp;&nbsp;
                                     <?=  Html::a('<i class="fa fa-trash"></i>', ['category-details/inactive?id='.$homedata['id']],['onclick' => 'return confirm("Are you sure you want to delete this item/s?")']);?>
                                   </center>
                              </div>
                              </div>
                            </details>
                              
                        </div>
                    </div>
                  <?php  } ?>
          </div>
          </div>


             <?= Html::a('<i class="fa fa-plus"></i>',['category-details/add-category-details?id='.$model['id']], ['title'=>'Add New Slide','class' => 'btn btn-warning']) ?>
              </div>
              <div class="box-footer">
                <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
               <?= Html::a('Back',['category/index'],['class' => 'btn btn-default'])?>
              </div>

        </div>       
            <?php ActiveForm::end(); ?>
    </div>
</div>
    <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog" style="min-width:630px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style=" margin-top:30px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="padding-top:30px;">
                       
                          <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
   function fileuploadHandler(e, data){
     console.log('upload done');
     console.log( data._response.result.files[0].url);
      
   }

   </script>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">

CKEDITOR.replace('homemain-home_intro', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink'] },
        
       {name: 'colors', items : ['TextColor', 'BGColor']},
        { name: 'styles', items: [ 'Format'] },

    ],
    toolbarGroups: [
       
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );


  
$("#submit_lower").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$("#submit_upper").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
function validateExtension(id) {

    //Get reference of FileUpload.
    var fileUpload = document.getElementById(id);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.mp3)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 10) { // make it in MB so divide by 1024*1024
                    alert('Please select audio file size less than 10 MB');
                    return false;
                    }
                    else
                    {
                      var height = this.height;
                      var width = this.width;
                      return true;
                    }
                    
                   
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid audio file.");
        fileUpload.value="";
        return false;
    }
}


function validateImage(id) {

    var fileUpload = document.getElementById(id);

    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
         if (typeof (fileUpload.files) != "undefined") {
            var reader = new FileReader();
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                var image = new Image();
                 image.src = e.target.result;
                  image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 2) { 
                    alert('Please select image size less than 2 MB');
                    return false;
                    }
                    else
                    {
                        var height = this.height;
                        var width = this.width;
                   
                        $('#preview_profile_photo').attr('src', e.target.result);

                        return true;
                    }  
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid Image file.");
            fileUpload.value="";
        return false;
    }
}  


function validateImage1(id) {
    var fileUpload = document.getElementById(id);
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
         if (typeof (fileUpload.files) != "undefined") {
            var reader = new FileReader();
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                var image = new Image();
                 image.src = e.target.result;
                  image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 2) { 
                    alert('Please select image size less than 2 MB');
                    return false;
                    }
                    else
                    {
                      var height = this.height;
                      var width = this.width;
                      $('#preview_profile_photo1').attr('src', e.target.result);
                      return true;
                    }  
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid Image file.");
            fileUpload.value="";
        return false;
    }
}
</script>



</div>
