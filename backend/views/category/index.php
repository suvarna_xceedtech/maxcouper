<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HomeMainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category';
$this->params['breadcrumbs'][] = $this->title;
$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'multipledelete\',
    data : {row_id: HotId},
    success : function(response) {
        
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>

<div class="podcast-index">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i>  Category List</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
               <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
               <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?>
            </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                 [
                    'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                            return ['value' => $data->id];
                    },
                ],   
                [
                  'attribute'=>'title',
                  'format' => 'html',
                  'value'=>function($data) {
                            // return '<img src="'.$data->image.'" class="img-responsive" style="width:70px;" />';
                            return $data->title;
                            
                    },
                ],

                 




                 //'home_intro:ntext',
                 
                //  [
                //   'attribute'=>'category_id',
                //   'format' => 'raw',
                //   'value'=>function($data) {
                //       // return Html::a(Html::encode($model->title),['your-journey/view?id='.$model['id']]);
                //     $category_name=Category::find()->where(['status'=>1,'id'=>$data->category_id])->one();
                //      //print_r($itemtenders['title']);die;
                //     return '<p>'.$category_name->title.'</p>';
                //   },
                // ],

               
                
                [
                    'header'=>'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                       return Html::a('<i class="fa fa-pencil"></i>', ['category/update?id='.$model['id']]);
                                     
                    },
                ],
            ],
        ]); ?>
      </div>
    </div>

</div>
<!-- </div></div> -->