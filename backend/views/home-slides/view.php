<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HomeSlides */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Home Slides', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="home-slides-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'home_main_id',
            'image',
            'image_alt',
            'title',
            'title_position',
            'top_description:ntext',
            'middle_description:ntext',
            'bottom_description:ntext',
            'sequence',
            'meta_title',
            'meta_keyword',
            'created_at',
            'updated_at',
            'updated_by',
            'status',
            'created_by',
            'alias',
        ],
    ]) ?>

</div>
