<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HomeSlidesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Slides';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-slides-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Home Slides', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'home_main_id',
            'image',
            'image_alt',
            'title',
            //'title_position',
            //'top_description:ntext',
            //'middle_description:ntext',
            //'bottom_description:ntext',
            //'sequence',
            //'meta_title',
            //'meta_keyword',
            //'created_at',
            //'updated_at',
            //'updated_by',
            //'status',
            //'created_by',
            //'alias',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
