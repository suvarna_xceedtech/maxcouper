<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryDetails */

$this->title = 'Category Details';
$this->params['breadcrumbs'][] = ['label' => 'Category Details', 'url' => ['category/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-details-create">

  <!--   <h1><?= Html::encode($this->title) ?></h1>
 -->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
