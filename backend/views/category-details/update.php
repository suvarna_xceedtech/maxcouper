<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryDetails */

$this->title = 'Category Details ';
$this->params['breadcrumbs'][] = ['label' => 'Category Details', 'url' => ['category/index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-details-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
