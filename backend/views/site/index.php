
<?php
use backend\models\Vehicles;
use backend\models\Company;
use backend\models\Transporter;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Dashboard';

?>

<!-- <div class="site-index">
    
    <div class="body-content">
    
      <div class="card">
        <div class="card-body"> -->
           <!--  <div class="card-title d-flex justify-content-between align-items-center">
                
                <div class="slick-single-arrows">
                    <a class="btn btn-outline-info  btn-sm">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="btn btn-outline-info btn-sm">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <div class="row slick-single-item"> 
            	<div class="col-md-2 grid-margin stretch-card">
		          <a href="#">
		            <div class="card border-0 border-radius-2 bg-success">
		              <div class="card-body">
		                <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
		                  <div class="icon-rounded-inverse-success icon-rounded-lg icon">
		                    <img src="<?= Yii::$app->params['image'];?>images/icons/celebrity-yellow.svg" />
		                  </div>
		                  <div class="text-white m-auto">
		                    <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
		                      <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1">Article</h3>
		                    </div>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </a>
		        </div>
                <div class="col-md-2 grid-margin stretch-card">
		          <a href="#">
		            <div class="card border-0 border-radius-2 bg-success">
		              <div class="card-body">
		                <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
		                  <div class="icon-rounded-inverse-success icon-rounded-lg icon">
		                    <img src="<?= Yii::$app->params['image'];?>images/icons/celebrity-yellow.svg" />
		                  </div>
		                  <div class="text-white m-auto">
		                    <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
		                      <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1">FAQ's</h3>
		                    </div>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </a>
		        </div>
		        <div class="col-md-2 grid-margin stretch-card">
		          <a href="#">
		            <div class="card border-0 border-radius-2 bg-success">
		              <div class="card-body">
		                <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
		                  <div class="icon-rounded-inverse-success icon-rounded-lg icon">
		                    <img src="<?= Yii::$app->params['image'];?>images/icons/celebrity-yellow.svg" />
		                  </div>
		                  <div class="text-white m-auto">
		                    <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
		                      <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1">Birthday Stars</h3>
		                    </div>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </a>
		        </div>
              	<div class="col-md-2 grid-margin stretch-card">
		          <a href="#">
		            <div class="card border-0 border-radius-2 bg-success">
		              <div class="card-body">
		                <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
		                  <div class="icon-rounded-inverse-success icon-rounded-lg icon">
		                    <img src="<?= Yii::$app->params['image'];?>images/icons/celebrity-yellow.svg" />
		                  </div>
		                  <div class="text-white m-auto">
		                    <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
		                      <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1">Celebrity Profile</h3>
		                    </div>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </a>
		        </div>
              	
              	
            </div> -->
           <!--  <div class="row">
                <div class="col-lg-3 col-xs-6"> -->
                  <!-- small box -->
                  <!-- <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3>Daily</h3>

                      <p>Excel Upload</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-file-excel-o"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to(['daily-upload/create'])?>" class="small-box-footer">
                      Click Here <i class="fa fa-arrow-circle-right"></i>
                    </a>
                  </div>
                </div> -->
               <!--  <div class="col-lg-3 col-xs-6">
                  small box
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3>Weekly</h3>

                      <p>Excel Upload</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-file-excel-o"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to(['weekly-upload/create'])?>" class="small-box-footer">
                      Click Here <i class="fa fa-arrow-circle-right"></i>
                    </a>
                  </div>
                </div>
                <div class="col-lg-3 col-xs-6"> -->
                  <!-- small box -->
                  <!-- <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3>Monthly</h3>

                      <p>Excel Upload</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-file-excel-o"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to(['monthly-upload/create'])?>" class="small-box-footer">
                      Click Here <i class="fa fa-arrow-circle-right"></i>
                    </a>
                  </div>
                </div> -->
                <!-- <div class="col-lg-3 col-xs-6">
                  small box
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3>Yearly</h3>

                      <p>Excel Upload</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-file-excel-o"></i>
                    </div>
                    <a href="<?= \yii\helpers\Url::to(['yearly-upload/create'])?>" class="small-box-footer">
                      Click Here <i class="fa fa-arrow-circle-right"></i>
                    </a>
                  </div>
                </div>
            </div> -->
        <!-- </div>
    </div>
  </div>
</div> -->
 
 <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
  <script src='<?php echo Yii::$app->params['image']?>js/slick.min.js'></script>
  <script type="text/javascript">
  	
  	$(document).ready(function () { 
     if ($('.slick-single-item').length) {
        $('.slick-single-item').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            prevArrow: '.slick-single-arrows a:eq(0)',
            nextArrow: '.slick-single-arrows a:eq(1)',
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
  });
  </script>
  <style type="text/css">
      .bg-yellow{
        background: #E79E44;
      }
  </style>