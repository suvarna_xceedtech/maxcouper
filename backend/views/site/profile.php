<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
// $starsign=\backend\models\Starsign::find()->where(['status'=>1])->all();

?>
<div class="row gutters-sm">
  <div class="col-md-4 mb-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-column align-items-center text-center">
          <?php if($model->profile_pic=="" && $model->profile_pic==null) {
            echo '<img src="'.Yii::$app->params['server'].'images/user.jpg" alt="Admin" class="rounded-circle" width="150">';
            }
            else
              {
                echo '<img src="'.Yii::$app->params['server'].$model->profile_pic.'" alt="Admin" class="rounded-circle" width="150">';
              }?>
          <div class="mt-3">
            <h4><?= $model->first_name?></h4>
            <p class="text-secondary mb-1">Administrator</p><br />
             <a class="btn btn-primary" href="<?= \yii\helpers\Url::to(['site/edit-profile','id'=>Yii::$app->user->identity->id])?>"><i class="fa fa-pencil"></i> Edit Profile</a>
          </div>
        </div>
      </div>
    </div>
    
  </div>
 
</div>
       

<style type="text/css">
  .nav{display:block;}

</style>