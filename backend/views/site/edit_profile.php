<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;

$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
/* @var $this yii\web\View *//* @var $model backend\models\About */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>

<div class="about-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Add Details</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['id'=>'submit_upper','class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'class'=>'form-control', 'readonly'=>true]) ?>
                </div>
                <div class="form-group">
                  <?php $value="";
                      if(!$model->isNewRecord)
                      {
                        $value=($model->birth_date!="0000-00-00" && $model->birth_date!=null) ? date('d-m-Y', strtotime($model->birth_date)): "";
                      }      
                        echo $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
                              'name' => 'birth_date', 
                              'options' => [//'placeholder' => 'Select issue date ...'
                              'value'=>$value,
                                      ],
                              'pluginOptions' => [
                                  'format' => 'dd-mm-yyyy',
                                  'todayHighlight' => true
                              ]
                          ]);
                    ?>
                </div>
                <div class="form-group">
                    <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                    <?php echo $form->field($model, 'profile_pic')->fileInput(['class'=>'form-element'])->label(false) ?>

                                   
                                </div>
                            </div>
                            <div id="edit-field-image-0--description" class="form-item__description">
                            One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.<br>Image Size: 195x250
                        </div>
                      </div>
                      </div>
                    </details>
                </div>
                
                
            </div>
            <div class="col-md-5">
                <div class="entity-meta accordion js-form-wrapper form-wrapper" id="edit-advanced">
                  <div class="entity-meta__header accordion__item js-form-wrapper form-wrapper" id="edit-meta">
                      
                      <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                         <label for="edit-meta-changed" class="form-item__label">Last saved</label>
                         Not saved yet
                      </div>
                      <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                        <label for="edit-meta-author" class="form-item__label">Author</label>
                         <?= $authorname;?>
                      </div>
                      <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                        <label for="edit-meta-author" class="form-item__label">Creation Date</label>
                        <?= $form->field($model, 'created_at')->hiddenInput(['value' => $model->created_at])->label(false); ?>

                         <?= date_format(date_create($model->created_at),'d-m-Y H:i:s');?>
                      </div>
                  </div>
                
                </div>
            </div>
        </div>
       </div>
       <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog" style="min-width:630px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style=" margin-top:30px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="padding-top:30px;">
                       
                          <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:220,
      height:220,
      type:'square' //circle
    },
    boundary:{
      width:200,
      height:200
    }
  });
  $('#user-profile_pic').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });



  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('#uploadimageModal').modal('hide');
        $("#preview_profile_photo").attr("src",response);
        $("#user-photo").val(response);
     
    })
  });

});  
$("#submit_lower").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$("#submit_upper").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});

</script>