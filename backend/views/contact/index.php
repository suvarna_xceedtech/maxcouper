<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;

$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'multipledelete\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>
<div class="podcast-index">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i>  Contact List</h3>
        </div>
       
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                
                [
                  'attribute'=>'title',
                  'format' => 'raw',
                  'value'=>function($data) {
                      // return Html::a(Html::encode($model->title),['your-journey/view?id='.$model['id']]);
                    return '<p>'.$data->title.'</p>';
                  },
                ],
                // 'description:ntext',


                [
                  'attribute'=>'description',
                  'format' => 'html',
                  'value'=>function($data) {
                    $small = substr($data->description, 0, 300);
                            return '<p>'.$small.'</p>';
                    },
                ],
                [
                  'attribute'=>'image',
                  'format' => 'html',
                  'value'=>function($data) {
                            // return '<img src="'.$data->image.'" class="img-responsive" style="width:70px;" />';
                            return '<img src="'.Yii::$app->params['server'].''.$data->image.'" class="img-responsive" style="width:70px;" />';

                            
                    },
                ],

               

                [
                    'attribute'=>'alt',
                    'format' => 'html',
                    'value' => function ($data) {
                       return '<p>'.$data->alt.'</p>';
                    },
                ],
                [
                    'header'=>'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                       return Html::a('<i class="fa fa-pencil"></i>', ['contact/update?id='.$model['id']]);
                      // return Html::a('<i class="fa fa-trash"></i>', ['podcast/delete?id='.$model['id'].'&flag=1'], ['data-method'=>'POST']) ;                    
                    },
                ],
            ],
        ]); ?>
      </div>
    </div>

</div>
