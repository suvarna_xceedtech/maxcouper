<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;


$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
/* @var $this yii\web\View */
/* @var $model backend\models\Podcast */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>

<div class="podcast-form">
    <div class="card">
        <div class="card-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
              <div class="box-header with-border">
                <div class="col-md-8">
                    <h3 class="box-title"> <i class="fa fa-pencil"></i> Update Contact</h3>
                </div>
                <div class="col-md-4">
                    <div class="pull-right">
                        <?= Html::submitButton('Save', ['id'=>'submit_upper','class' => 'btn btn-success']) ?>
                        
                    </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                            
                        </div>
                         <div class="form-group">
                            <?= $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>
                            
                        </div>
                       
                        <div class="form-group">
                            <?php echo $form->field($model, 'description')->textArea([ 'class'=>'form-control']) ?>
                        </div>

                      <!--   <div class="form-group">
                            <?= $form->field($model, 'contact_mobile')->textInput(['maxlength' => true]) ?>
                            
                        </div>   -->                    
                 
                       

                        <div class="form-group">
                            <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                            
                        </div>

                        <div class="form-group">
                            <?= $form->field($model, 'alias')->textInput(['maxlength' => true,'readOnly' => true]) ?>
                            
                        </div>
                         <div class="form-group">
                            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
                            
                        </div>

                        <div class="form-group">
                            <?= $form->field($model, 'meta_keyword')->textInput(['maxlength' => true]) ?>
                            
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
                            
                        </div>
                        
                        <div class="form-group">
                            <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                                  <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                                  <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                                    <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                        <div class="form-managed-file__main">
                                            <?php echo $form->field($model, 'image')->fileInput(['class'=>'form-element', 'onchange'=>'validateImage(this.id)'])->label(false) ?>

                                           <!-- <?= $form->field($model, 'image')->hiddenInput()->label(false); ?> -->
                                        </div>
                                    </div>
                                    <div id="edit-field-image-0--description" class="form-item__description">
                                    One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.<br>Image Size: 195x250
                                </div>
                              </div>
                              </div>
                            </details>
                        </div>
                         <div class="form-group">
                            <?= $form->field($model, 'alt')->textInput(['maxlength' => true]) ?>
                            
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="entity-meta accordion js-form-wrapper form-wrapper" id="edit-advanced">
                          <div class="entity-meta__header accordion__item js-form-wrapper form-wrapper" id="edit-meta">
                              <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                                <label for="edit-meta-changed" class="form-item__label">Image Preview</label>
                                <div class="layout__region layout__region--first">
                                  <div class="block-with-background block">
                                    <figure>   
                                    <?php if(!isset($model->image)){?>
                                      <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                        <img src="<?= Yii::$app->params['server'];?>images/preview.png" id="preview_profile_photo" name="preview_profile_photo">

                                      </div>
                                      <?php }else{?>
                                          <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                        <img src="<?= Yii::$app->params['server'];?><?php echo $model->image;?>" id="preview_profile_photo" name="preview_profile_photo">

                                      </div>

                                     <?php } ?>
                                    </figure>
                                  </div>
                                </div>
                              </div>
                              <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                                 <?php if(empty($model->updated_at)){?>
                                 <label for="edit-meta-changed" class="form-item__label">Last saved</label>
                                 Not saved yet
                               <?php }else{?>

                                <label for="edit-meta-changed" class="form-item__label">Last saved</label> <?= $model->updated_at;?>
                              <?php }?>
                              </div>
                              <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                                <label for="edit-meta-author" class="form-item__label">Author</label>
                                 <?= $authorname;?>
                              </div>
                              <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                                <label for="edit-meta-author" class="form-item__label">Creation Date</label>
                                <?= $form->field($model, 'created_at')->hiddenInput(['value' => $model->created_at])->label(false); ?>

                                 <?= $model->created_at;?>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>


              </div>
              <div class="box-footer">
                <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
                 <?= Html::a('Back',['contact/index'],['class' => 'btn btn-default'])?>
              </div>

        </div>       
            <?php ActiveForm::end(); ?>
    </div>
</div>
    <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog" style="min-width:630px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style=" margin-top:30px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="padding-top:30px;">
                       
                          <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
   function fileuploadHandler(e, data){
     console.log('upload done');
     console.log( data._response.result.files[0].url);
      
   }

   </script>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>

<script type="text/javascript">
CKEDITOR.replace('contact-description', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink'] },
        
       {name: 'colors', items : ['TextColor', 'BGColor']},
        { name: 'styles', items: [ 'Format'] },

    ],
    toolbarGroups: [
       
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );
$(document).ready(function(){
 
  $("#contact-title").keyup(function()
  {
    var key=$("#contact-title").val();
     key=key.split(' ').join('-');
     key=key.split('.').join('-');
     key=key.split('&').join('');
     key=key.split('$').join('-');
     key=key.split('#').join('-');
     key=key.split('!').join('-');
     key=key.split('#').join('-');    
     key=key.split('%').join('-');
     key=key.split('^').join('-');
     key=key.split('*').join('-');
     key=key.split('(').join('-');
     key=key.split(')').join('-');
     key=key.split('+').join('-');
     key=key.split('_').join('-');
     key=key.split('"').join('-');
     key=key.split('/').join('-');
     key=key.split('<').join('-');
     key=key.split('>').join('-');
     key=key.split('|').join('-');
     key=key.split('?').join('-');
     key=key.split(',').join('-');
     key=key.split('`').join('-');
     key=key.split('{').join('-');
     key=key.split('}').join('-');
     key=key.split('[').join('-');
     key=key.split(']').join('-');
     key=key.split(':').join('-');
     key=key.split(';').join('-');
     key=key.split('~').join('-');
     key=key.split('!').join('-');
     key=key.split('@').join('-');
     key=key.split('=').join('-');
     key=key.split('/').join('-');

    $("#contact-alias").val(key);
  });

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:280,
      height:265,
      type:'square' //circle
    },
    boundary:{
      width:270,
      height:255
    }
  });
  $('#podcast-image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });



  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('#uploadimageModal').modal('hide');
        $("#preview_profile_photo").attr("src",response);
        $("#podcast-photo").val(response);
     
    })
  });

});  
$("#submit_lower").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$("#submit_upper").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
function validateExtension(id) {

    //Get reference of FileUpload.
    var fileUpload = document.getElementById(id);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.mp3)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 10) { // make it in MB so divide by 1024*1024
                    alert('Please select audio file size less than 10 MB');
                    return false;
                    }
                    else
                    {
                      var height = this.height;
                      var width = this.width;
                      return true;
                    }
                    
                   
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid audio file.");
        fileUpload.value="";
        return false;
    }
}
</script>


</div>
