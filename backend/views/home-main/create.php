<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HomeMain */

$this->title = 'Home';
$this->params['breadcrumbs'][] = ['label' => 'Home', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-main-create">

   <!--  <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
