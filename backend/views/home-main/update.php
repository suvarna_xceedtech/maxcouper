<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HomeMain */

$this->title = 'Home Main';
$this->params['breadcrumbs'][] = ['label' => 'Home Mains', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="home-main-update">

   <!--  <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
