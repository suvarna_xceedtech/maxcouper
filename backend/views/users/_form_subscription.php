<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\User;
use yii\db\Query;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model backend\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="role-form">

    <?php $form = ActiveForm::begin([]); ?>
    <div class="box box-primary" style="border:0">
    
      <div class="box-body">
        <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php   
                        $employee = \backend\models\Users::find()->where(['status'=>1, 'user_type'=>0])->all();
                                $countryData=ArrayHelper::map($employee,'id','first_name');
                                echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                                'data' => $countryData,
                                
                                'options' => ['placeholder' => '-- Select --'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <?php   
                        $employee = \backend\models\Subscription::find()->all();
                                $countryData=ArrayHelper::map($employee,'id','duration');
                                echo $form->field($model, 'item_id')->widget(Select2::classname(), [
                                'data' => $countryData,
                                
                                'options' => ['placeholder' => '-- Select --', 'onchange'=>'return getduration(this)'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <?php
                           $data = \backend\models\Starsign::find()->where(['status'=>1])->andWhere(['<>','id',"-1"])->all();
                                $starsignData=ArrayHelper::map($data,'title','title');
                           echo $form->field($model, 'starsign')->widget(Select2::classname(), [
                                'data' => $starsignData,
                                
                                'options' => ['placeholder' => '-- Select --'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                           ?>
                    </div>
                </div>
                
                
              </div>
              <div class="row">
                <div class="col-md-4" style="padding-top:20px;padding-bottom:20px;">
                    <input type="checkbox" name="extra_months" id="extra_months" checked>
                    <label for="extra_months">  Add extra 6 months subscription</label>
                    <input type="hidden" name="duration" id="duration" >

                </div>
                <div class="col-md-4">
                    <div class="form-group">

                       <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                              'name' => 'start_date', 
                              'options' => [//'placeholder' => 'Select issue date ...'
                              'onchange'=>'return getEnddate(this)',
                                      ],
                              'pluginOptions' => [
                                  'format' => 'dd-mm-yyyy',
                                  'todayHighlight' => true
                              ]
                          ]);?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                              'name' => 'end_date', 
                              'options' => [//'placeholder' => 'Select issue date ...'
                              
                                      ],
                              'pluginOptions' => [
                                  'format' => 'dd-mm-yyyy',
                                  'todayHighlight' => true
                              ]
                          ]);?>
                    </div>
                </div>
                
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                      <?= $form->field($model, 'status')->widget(Select2::classname(), [
                                'data' => array('Pending'=>'Pending', 'Confirm'=>'Confirm'),
                                
                                'options' => ['placeholder' => '-- Select --',],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('Payment Status');
                        ?>
                  </div>
                </div>
              </div>
               
            </div>
          </div>
       </div>
       <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    </div>
   
    <?php ActiveForm::end(); ?>

</div>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>



<script>
  function getduration(ele)
  {
    var duration=0;
    var months=0;
    var val=$('#'+ele.id+' :selected').text();
    //$("#duration").val(val);
    var checkbox= $('#extra_months').is(':checked');
    if(checkbox)
    {
      months=6;  
    } 
    duration=parseInt(val)+months;
    $("#duration").val(duration);
    var date=$("#cart-start_date").val();
    if(date!="" && duration!=0)
    {
      $.ajax({
          type:'POST',
          data:{'date': date, 'duration': duration},
          url:"<?php echo yii\helpers\Url::to(['users/getenddate']) ?>",
          success:function(data)
          {
              $("#cart-end_date").val(data);
          }
      });
    }
    

  }
  $("#extra_months").change(function() {
    var duration=0;
    var months=0;
    var val=$('#cart-item_id :selected').text();
    if(this.checked) {
        months=6;
    }
    duration=parseInt(val)+months;
    $("#duration").val(duration);
    var date=$("#cart-start_date").val();
    if(date!="" && duration!=0)
    {
      $.ajax({
          type:'POST',
          data:{'date': date, 'duration': duration},
          url:"<?php echo yii\helpers\Url::to(['users/getenddate']) ?>",
          success:function(data)
          {
              $("#cart-end_date").val(data);
          }
      });
    }
    
  });
  function getEnddate(ele)
  {
    var date=ele.value;

    var duration=$("#duration").val();
    if(duration!="")
    {
        $.ajax({
            type:'POST',
            data:{'date': date, 'duration': duration},
            url:"<?php echo yii\helpers\Url::to(['users/getenddate']) ?>",
            success:function(data)
            {
                $("#cart-end_date").val(data);
               // alert(data);
            }
        });
    }
    
      
  }
  
</script>