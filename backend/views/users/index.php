<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use fedemotta\datatables\DataTables;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="<?= \yii\helpers\Url::to(['users/index']); ?>">User</a></li>
      <li><a href="<?php echo \yii\helpers\Url::to(['users/subscription']); ?>" >Add Subscription</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
          <div class="row setup-content" id="step-1">
           
                <div class="col-md-12">
                    <div class="card" style="border:0">
                        <div class="box-header">
                            <div class="col-md-6">
                                <h3 class="box-title"> <i class="fa fa-list"></i> All Users</h3>
                            </div>
                            <div class="col-md-6">
                              <div class="pull-right">
                                <?= Html::a('<i class="fa fa-plus"></i>', ['add'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                                <div class="btn-group">
                                    <button type="button" id="changebtntxt" class="btn btn-success" >All Users</button>
                                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a onclick='return (getstatuswise("-1"))' style='cursor:pointer'>All Users</a></li>
                                        <li><a onclick='return (getstatuswise(1))' style='cursor:pointer'>Active</a></li>
                                        <li><a onclick='return (getstatuswise(2))' style='cursor:pointer'>Inactive</a></li>
                                               
                                    </ul>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="user-index">
                                
                                <div class="row mt-2-2">
                                    <div class="col-md-12">
                                        <div id="result">
                                            <?= DataTables::widget([
                                              'dataProvider' => $dataProvider,
                                              'clientOptions' => [
                                                  'lengthMenu' => [200],
                                                  'responsive' => true, 
                                                  'dom' => 'BfTrtip',
                                                  'buttons' => [
                                                    'csv','excel', 'pdf', 'print'
                                                    ]
                                                ],
                                              'columns' => [
                                                 // ['class' => 'yii\grid\SerialColumn'],
                                                    //['class' => 'yii\grid\SerialColumn'],
                                                 
                                                     [
                                                        'attribute' => 'fullname',
                                                        'value' => function($data) { return $data->first_name  ." ". $data->last_name; }
                                                     ],
                                                    'username',
                                                    'starsign',
                                                    'email',
                                                    [
                                                        'attribute' => 'birth_date',
                                                        'value' => function ($model) { 
                                                               return ($model->birth_date!="0000-00-00")?date_format(date_create($model->birth_date),'d-m-Y'):"";
                                                        },
                                                    ],
                                                    [
                                                        'header' => 'Last Visit Date',
                                                        'value' => function ($model) { 
                                                               return getLastVisit($model->id);
                                                        },
                                                    ], 
                                                    [
                                                        'attribute' => 'created_at',
                                                        'header' => 'Registration Date',
                                                        'value' => function ($model) { 
                                                               return ($model->created_at!="0000-00-00")?date_format(date_create($model->created_at),'d-m-Y H:i:s'):"";
                                                        },
                                                    ], 
                                                    
                                                    [
                                                        'label'=> '#',
                                                        'format'=> 'raw',
                                                        'value' => function($model) { 
                                                             return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['users/changestatus?id='.$model->id.'&flag=2'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this user?')):Html::a('<i class="fa fa-check"></i>', ['users/changestatus?id='.$model->id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this user?'));
                                                        }
                                                    ],
                                                ],
                                          ]);?>
                                        
                                        </div>
                                        <div id="resultdata" style="display:none">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            
          </div>
          
      </div>
      <!-- /.tab-pane -->
     
      
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>

<input type="hidden" id="status" value="-1"/>
<?php
function getLastVisit($id)
{
    $login = \app\models\LoginDetails::find()->where(['login_user_id' => $id])->orderBy(['login_detail_id'=>SORT_DESC])->one();
    return (!empty($login))?date_format(date_create($login->login_at),'d-m-Y  H:i:s'):"Never";
}
?>

<script type="text/javascript">
    
function getstatuswise(flag)
{
    var status="";
    if(flag==1)
        status="Active";
    else if(flag==2)
        status="Inactive";
    else
        status="All Users";
    $("#status").val(flag);
   
    $("#changebtntxt").text(status);
        $.ajax({
            type:'POST',
            url:"<?php echo Url::to(['users/getstatuswiselisting']) ?>",
            data:{'status':flag},
            success:function(items)
            {
                //alert(items);
                $("#result").hide();
                $("#resultdata").show();
                $("#resultdata").html(items);
            }
        });
}
</script>

