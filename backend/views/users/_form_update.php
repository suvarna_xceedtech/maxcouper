<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Role;
use backend\models\Company;
use backend\models\Employee;
use yii\helpers\Url;
use app\models\User;
use backend\models\ModulesList;
use backend\models\UserModulePermission;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */
/* @var $form yii\widgets\ActiveForm */
if( \Yii::$app->session->hasFlash('msg'))
        echo \Yii::$app->session->getFlash('msg');
?>
<div class="role-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    <div class="row">
      <?php if(\Yii::$app->user->identity->role_id!=1)
        { echo $form->field($model, 'company_id')->hiddenInput(['value'=>\Yii::$app->user->identity->company_id])->label(false);
          $role=Role::find()->where(['status' => 1, 'company_id'=>\Yii::$app->user->identity->company_id])->all();
          $listData=ArrayHelper::map($role,'id','role_name');
            echo '<div class="col-md-3">';
                       
                 echo $form->field($model, 'role_id')->widget(Select2::classname(), [
                    'data' => $listData,
                    'options' => ['placeholder' => '-- Select --',
                     ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
            echo '</div>';
            $subQuery= User::find()->select('user_id')
                        ->where(['status'=>1, 'company_id'=>Yii::$app->user->identity->company_id]);
            $role=Employee::find()->where(['status' => 1, 'company_id'=>\Yii::$app->user->identity->company_id])->andWhere(['NOT',['id'=>$subQuery]])->all();
            $rolelistData=ArrayHelper::map($role,'id','concat(first_name." ".last_name) as name');
            echo '<div class="col-md-3">';
              echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => $listData,
                    'options' => ['placeholder' => '-- Select --',
                     ],
                    'pluginOptions' => [
                        'allowClear' => true,
                         "disabled"=>"disabled" 
                    ],
                ]);
            echo '</div>';
        }
        else
            {
              echo '<div class="col-md-3">';
                  $company=Company::find()->where(['status' => 1])->all();
                  $listData=ArrayHelper::map($company,'id','company_name');
                  echo $form->field($model, 'company_id')->widget(Select2::classname(), [
                        'data' => $listData,
                        'options' => ['placeholder' => '-- Select --',
                         'onchange'=>'
                            $.get( "'.Url::toRoute('/users/role').'", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#'.Html::getInputId($model, 'role_id').'" ).html( data );
                                }
                            );
                        '    ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            "disabled"=>"disabled" 

                        ],
                    ]); 
                echo '</div>';
                $role=Role::find()->where(['status' => 1, 'company_id'=>$model->company_id])->all();
                $listData=ArrayHelper::map($role,'id','role_name');
                echo '<div class="col-md-3">';
               
                 echo $form->field($model, 'role_id')->widget(Select2::classname(), [
                    'data' => $listData,
                    'options' => ['placeholder' => '-- Select --',
                         'onchange'=>'
                            $.get( "'.Url::toRoute('/users/employee').'", { id: $("#users-company_id").val() } )
                                .done(function( data ) {
                                    $( "#'.Html::getInputId($model, 'user_id').'" ).html( data );
                                    getpermission(this);
                                }
                            );
                        ' 
                     ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                echo '</div>';   
                $subQuery= User::find()->select('user_id')
                        ->where(['status'=>1, 'company_id'=>$model->company_id]);
                $employee=User::find()->where(['status' => 1, 'company_id'=>$model->company_id, 'user_id'=>$model->user_id])->all();
                $emplistData=ArrayHelper::map($employee,'user_id',function($employee) {
                            return $employee['first_name'].' '.$employee['last_name'];
                        });   
                echo '<div class="col-md-3">';
                 echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => $emplistData,
                    'options' => ['placeholder' => '-- Select --',
                     ],
                    'pluginOptions' => [
                        'allowClear' => true,
                         "disabled"=>"disabled" 
                    ],
                ]);
                echo '</div>';
            }?>
   </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group">

            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success mt-2-2', 'id'=>'btn-success']) ?>
            
            <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save my-float"> </i>'), ['id'=>'btn-success1', 'class' => 'btn btn-success float']) ?>
        </div>
    </div>
  
  <div class="row">
    <div class="col-md-12">
        <div id='indirect-issue-grid' class='grid-view'>
            <?php
            echo "<div id='indirect-issue-grid' class='grid-view'>
                <table style='width:100%;border-collapse: collapse;margin:0 auto;background:#5FB355' class='table table-bordered table-striped'>
                        <thead>
                        <tr>
                            <th style='width:35%'>Select all menus</th>
                            <th><input type='checkbox' id='allcb' name='allcb'/> </th>
                        </tr></thead>
                        <tbody>";
                $menuMaster=GenrateTopMenu($model->id);
                echo $menuMaster; 
                echo "</tbody></table></div>";
            ?>
        </div>
    </div>
  </div>
<?php ActiveForm::end();
        
function GenrateTopMenu($um_id)
{
    $topMenu="";
    $ParentMenuMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>0, 'show_in_sidebar'=>1])->orderBy(['sequence'=>SORT_ASC])->all();
    
    foreach($ParentMenuMaster as $parentModel)
    {
        //print_r($parentModel->menu_id);
        $topMenu.="<table style='width:100%;border-collapse: collapse;margin:0 auto;table-layout: fixed;
' class='table table-bordered table-striped'>";
        
        $topMenu.=CreateTopMenuChild($parentModel->module_id, $parentModel->module_name, $parentModel->url,$parentModel, $um_id);
        $topMenu.="</table>";
    }
    return $topMenu;

}
function CreateTopMenuChild($menuId, $menuName, $menuUrl,$parentModel, $um_id)
{
    $topMenu="";
    $ChildModelMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>$menuId])->groupBy(['group_name'])->orderBy(['sequence'=>SORT_ASC])->all();
    if (count($ChildModelMaster) == 0 )
    {
        
        $topMenu.= "<tr class='top-row' ><td style='width:35%; align:right;align-right:0px;'><input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$parentModel->module_id."-ck'); /> <b>". ucfirst($parentModel->group_name).":</b></td><td style='width:65%; align:right;align-right:0px;'><div class='row' style=''>";
         $ChildMenulMaster=ModulesList::find()->where(['is_active' => 1, 'group_name'=>$parentModel->group_name])->orderBy(['sequence'=>SORT_ASC])->all();
         if(!empty($ChildMenulMaster))
         {

            foreach ($ChildMenulMaster as $ChildGroup) {
                $topMenu.="<div class='col-md-4'><span style='align:center'>".getAccessNew($parentModel->parent_id,$ChildGroup->module_id, $parentModel->module_id, $um_id)." ".ucfirst($ChildGroup->module_name)."</span></div>";
            }
         }
        $topMenu.= "</div></td></tr>";
    }
    else
    {
        
        $topMenu.= "<tr class='sub-row'><td style='width:35%;'><input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$menuId."-ck'); /> <b>". $menuName."</b></td><td style='width:65%;'>";
      //  $topMenu.= "<input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$menuId."-ck'); />";
        $topMenu.="</td></tr>";
        /* Top Menu Childs */

        foreach ($ChildModelMaster as $ChildModel)
        {
            $topMenu.=CreateTopMenuChild($ChildModel->module_id, $ChildModel->module_name, $ChildModel->url,$ChildModel, $um_id);
        }

    }
    return $topMenu;
}

     
function getAccessNew($menuId, $id, $parentId, $um_id)
{
    $UserMenuAccess=UserModulePermission::find()->where(['user_id'=>$um_id,'module_id'=>$id])->one();
    if(!empty($UserMenuAccess) && $UserMenuAccess->access!=0)
    {
        return ("<input type='checkbox' name=$id  class='".$menuId."-ck ".$parentId."-ck' checked=true value='OK'></input>");
    }
    else
    {
        return ("<input type='checkbox' name=$id  class='".$menuId."-ck ".$parentId."-ck'></input>");
    } 
}

?>



</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script type="text/javascript">
  
  function getpermission(ele)
  {
    var val=$('#users-role_id :selected').val()
    $.ajax({
            type:'POST',
            url:"<?php echo Url::to(['users/getrolepermissions']) ?>",
            data:{'role_id': val},
            success:function(items)
            {
             

              $("#indirect-issue-grid").html(items);
              

            }

        });
  }
</script>

<script>
function checkedAll()
{
  if($("#allcb").prop('checked')){
        $('tbody tr td input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
        });
    }else{
        $('tbody tr td input[type="checkbox"]').each(function(){
            $(this).prop('checked', false);
        });
    }
}
function checkmenu(ele)
{
  if($('#' + ele).is(":checked"))
    $('.'+ele).each(function() { $(this).prop('checked', true) });
  else
    $('.'+ele).each(function() { $(this).prop('checked', false) });
}
</script>