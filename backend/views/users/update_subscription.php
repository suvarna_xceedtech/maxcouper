<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User 
<h1><?= Html::encode($this->title) ?></h1>*/

$this->title = 'Update Subscription';
$this->params['breadcrumbs'][] = ['label' => 'Subscription', 'url' => ['subscription']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li><a href="<?= \yii\helpers\Url::to(['users/index']); ?>">User</a></li>
              <li  class="active"><a href="<?php echo \yii\helpers\Url::to(['users/subscription']); ?>" >Add Subscription</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                  <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                          <?= $this->render('_form_update_subscription', [
          						        'model' => $model,
          						        //'user_type'=>$user_type
          						    ]) ?>
                        </div>
                    </div>
                  </div>
                  
              </div>
              <!-- /.tab-pane -->
             
              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

    

</div>
