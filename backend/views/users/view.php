<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\ModulesList;
use backend\models\UserModulePermission;
use backend\models\Role;


/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title='Summary of '. $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->first_name." ".$model->last_name;
\yii\web\YiiAsset::register($this);
function getRole($role_id)
{

    $role="";
    $Role=Role::findOne($role_id);
    if($Role!="")
        $role=$Role->role_name;
    return $role;
}
?>
<div class="card">
        <div class="card-body p-50">
<div class="user-view">
   
 
<div class="row">
    <div class="col-md-3">
      <!-- Profile Image -->
      <div class="box box-primary" style="border-top-color: #d2d6de;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
        <div class="box-body box-profile">

            <i class="profile-user-img role-name img-responsive img-circle"><?=mb_substr($model->first_name, 0, 1, "UTF-8")?></i>
         
          <h3 class="profile-username text-center"><?= $model->first_name." ".$model->last_name?></h3>
          <p class="text-muted text-center"><small>Role: <?= getRole($model->role_id)?></small></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Email ID</b> <a class="pull-right"><?= $model->email_id?></a>
            </li>
            <li class="list-group-item">
              <b>Mobile</b> <a class="pull-right"><?= $model->mobile_no?></a>
            </li>
            <li class="list-group-item">
              <b>Username</b> <a class="pull-right"><?= $model->username?></a>
            </li>
          </ul>
          <?= Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'update')?Html::a('<i class="fa fa-pencil-square-o"></i> Edit User', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-block']):"" ?>
          <?= Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'delete')? Html::a('<i class="fa fa-trash"></i> Delete User', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-block',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]):"" ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <div class="col-md-9">
        <?php
            $menuMaster=GenrateTopMenu($model->user_id);
            echo $menuMaster; 
        ?>     
    </div>
</div>

<?php
function GenrateTopMenu($um_id)
{
    $topMenu="";
    $ParentMenuMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>0, 'show_in_sidebar'=>1])->orderBy(['sequence'=>SORT_ASC])->all();
    
    foreach($ParentMenuMaster as $parentModel)
    {
        //print_r($parentModel->menu_id);
        $topMenu.="<table style='width:100%;border-collapse: collapse;margin:0 auto;table-layout: fixed;
' class='table table-bordered table-striped'>";
        //echo $um_id."<br />";
        $topMenu.=CreateTopMenuChild($parentModel->module_id, $parentModel->module_name, $parentModel->url,$parentModel, $um_id);
        $topMenu.="</table>";
    }
    return $topMenu;

}
function CreateTopMenuChild($menuId, $menuName, $menuUrl,$parentModel, $um_id)
{

    $topMenu="";
    $ChildModelMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>$menuId])->groupBy(['group_name'])->orderBy(['sequence'=>SORT_ASC])->all();
    if (count($ChildModelMaster) == 0 )
    {
        
        $topMenu.= "<tr class='top-row' ><td style='width:25%; align:right;align-right:0px;'> <b>". ucfirst($parentModel->group_name).":</b></td><td style='width:75%; align:right;align-right:0px;'><div class='row'>";
         $ChildMenulMaster=ModulesList::find()->where(['is_active' => 1, 'group_name'=>$parentModel->group_name])->orderBy(['sequence'=>SORT_ASC])->all();
         if(!empty($ChildMenulMaster))
         {

            foreach ($ChildMenulMaster as $ChildGroup) {

                $topMenu.="<div class='col-md-4'><span style='align:center'>".getAccessNew($ChildGroup->module_id, $um_id)." ".ucfirst($ChildGroup->module_name)."</span></div>";
            }
         }
        $topMenu.= "</div></td></tr>";
    }
    else
    {
        
        $topMenu.= "<tr class='sub-row' ><td  style='background:#FAB529;width:25%;border:0' ><b>". $menuName."</b></td><td style='background:#FAB529;width:75%;border:0'></td></tr>";
     
        /* Top Menu Childs */

        foreach ($ChildModelMaster as $ChildModel)
        {
            $topMenu.=CreateTopMenuChild($ChildModel->module_id, $ChildModel->module_name, $ChildModel->url,$ChildModel, $um_id);
        }

    }
    return $topMenu;
}

function getAccessNew($id,$um_id)
{
    $UserMenuAccess=UserModulePermission::find()->where(['user_id'=>$um_id,'module_id'=>$id])->one();
    
    if(!empty($UserMenuAccess) && $UserMenuAccess->access!=0)
    {
        //return 1;
        return ("<i class='fa fa-check' style='color:#5fb355'></i>");
    }
    else
    {
        //return 0;
        return ("<i class='fa fa-close' style='color:#5fb355'></i>");
    }
}

?>