<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Users;
use backend\models\Cart;
use fedemotta\datatables\DataTables;
/* @var $this yii\web\View */
/* @var $model app\models\User 
<h1><?= Html::encode($this->title) ?></h1>*/

$this->title = 'Add Subscription';
$this->params['breadcrumbs'][] = ['label' => 'Subscription', 'url' => ['subscription']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li><a href="<?= \yii\helpers\Url::to(['users/add']); ?>">User</a></li>
              <li  class="active"><a href="<?php echo \yii\helpers\Url::to(['users/subscription']); ?>" >Add Subscription</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                  <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <div class="Orders-index">
                               <div class="box box-primary" style="border:0">
                                  <div class="box-header with-border">
                                    <div class="col-md-6">
                                        <h3 class="box-title"> <i class="fa fa-list"></i> Subscription</h3>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="pull-right">
                                        <?= Html::a('<i class="fa fa-plus"></i>', ['add-subscription'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                                       
                                      </div>
                                    </div>
                                  </div>
                                   <div class="box-body">
                                    <?php //print_r($dataProvider);die;?>
                                      <?= DataTables::widget([
                                              'dataProvider' => $dataProvider,
                                              'clientOptions' => [
                                              'lengthMenu' => [200],
                                              'responsive' => true, 
                                              'dom' => 'BfTrtip',
                                              'buttons' => [
                                                'csv','excel', 'pdf', 'print'
                                              ]
                                             ],
                                              'columns' => [
                                                 // ['class' => 'yii\grid\SerialColumn'],
                                                'orderid',
                                                 [
                                                   'attribute' => 'id',
                                                   'format' => 'raw',
                                                    'value'=>function($model) {
                                                      return Html::a($model->id,['/users/update-subscription', 'id' => $model->id]);
                                                    },
                                                  ], 
                                                  [
                                                    'label'=>'Type',
                                                    'attribute'=>'id',
                                                    'format' => 'raw',
                                                    'value' => function ($data) {
                                                            return getItemType($data['item_id'],1);
                                                     },
                                                  ],
                                                         'starsign',
                                                         [
                                                            'label'=>'Subscription start date',
                                                            'attribute'=>'start_date',
                                                            'format' => 'raw',
                                                            'value' => function ($data) {
                                                                return date_format(date_create($data->start_date),'d-m-Y');
                                                             },
                                                          ], 
                                                          [
                                                            'label'=>'Subscription end date',
                                                            'attribute'=>'end_date',
                                                            'format' => 'raw',
                                                            'value' => function ($data) {
                                                                return date_format(date_create($data->end_date),'d-m-Y');
                                                             },
                                                          ], 
                                                         [
                                                            'label'=>'Email',
                                                            'attribute'=>'user_id',
                                                            'format' => 'raw',
                                                            'value' => function ($data) {
                                                                    return getUsername($data['user_id'], 1);
                                                             },
                                                          ],                      
                                                          [
                                                            'label'=>'Name',
                                                            'attribute'=>'user_id',
                                                            'format' => 'raw',
                                                            'value' => function ($data) {
                                                                    return getUsername($data['user_id'],2);
                                                             },
                                                          ],
                                                        
                                                          [
                                                              'label'=>'Amount',
                                                              'attribute'=>'id',
                                                              'format' => 'raw',
                                                              'value' => function ($data) {
                                                                      return getItemType($data['item_id'],2);
                                                               },
                                                          ],
                                                          //'payment_status',
                                                            
                                                          [
                                                              'attribute'=>'created_at',
                                                               'value' => function ($model) {
                                                                 return date_format(date_create($model->created_at),'d-m-Y'); 
                                                                },
                                                                'headerOptions' => ['style' => 'width:10%'],
                                                          ]
                                                          
                                                          
                                                      ],
                                                  ]);?>

                                    <?php 

                                    function getItemType($cart_id, $flag)
                                    {        
                                      $title="";$amount=0;   
                                        $Cart = \backend\models\Cart::find()->where(['id'=>$cart_id])->one();
                                        if($Cart->is_chart==0)
                                        {
                                             $data = \backend\models\Subscription::find()->where(['id'=>$Cart->item_id])->one();
                                             $title=$data->name;
                                             $amount=$data->amount;
                                        }
                                        else
                                        {
                                            $data = \backend\models\Charts::find()->where(['id'=>$model->item_id])->one();
                                            $title=$data->title;
                                            $amount=$data->price;
                                        }
                                        if($flag==1)
                                          return $title;
                                        else
                                          return "£" .$amount;
                                    } 
                                    function getAmount($payment_id)
                                    {
                                         $TblPayment = \backend\models\TblPayment::find()->where(['id'=>$payment_id])->one();
                                                return $TblPayment->amount." ".$TblPayment->currency_code;
                                    }
                                    function getUsername($user_id, $flag)
                                    {
                                      $Users = \backend\models\Users::find()->where(['id'=>$user_id])->one();
                                      if($flag==1)
                                        return $Users->email;
                                      else
                                        return $Users->first_name;
                                    }
                                    ?>
                                  </div> 
                            </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  
              </div>
              <!-- /.tab-pane -->
             
          </div>
            <!-- /.tab-content -->
        </div>

    

</div>
