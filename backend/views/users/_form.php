<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\User;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */
/* @var $form yii\widgets\ActiveForm */
if( \Yii::$app->session->hasFlash('msg'))
        echo \Yii::$app->session->getFlash('msg');
?>
<div class="role-form">

    <?php $form = ActiveForm::begin([]); ?>
    <div class="box box-primary" style="border:0">
    
      <div class="box-body">
        <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                      <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class'=>'form-control'])->input('email'); ?>
                      <?= $form->field($model, 'username')->hiddenInput(['maxlength' => true, 'class'=>'form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= $form->field($model, 'password')->textInput(['maxlength' => true,'value'=>'ch@ngey0urp@w0rd', 'class'=>'form-control']) ?>
                    </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group">
                         
                          <?=  $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
                              'name' => 'birth_date', 
                              'options' => [//'placeholder' => 'Select issue date ...'
                              
                                      ],
                              'pluginOptions' => [
                                  'format' => 'dd-mm-yyyy',
                                  'todayHighlight' => true
                              ]
                          ]);
                          ?>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form_group">
                       <?php
                          $data = \backend\models\Starsign::find()->where(['status'=>1])->andWhere(['<>','id',"-1"])->all();
                          $starsignData=ArrayHelper::map($data,'title','title');
                          echo $form->field($model, 'starsign')->widget(Select2::classname(), [
                                'data' => $starsignData,
                                
                                'options' => ['placeholder' => '-- Select --'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);?>
                    </div>
                  </div>
              </div>  
            </div>
          </div>
       </div>
       <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    </div>
   
    <?php ActiveForm::end(); ?>

</div>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>



<script>

$(function(){
  

  $("#users-email").keyup(function()
  {
    var key=$("#users-email").val();
    

    $("#users-username").val(key.toLowerCase());
  });
});
</script>