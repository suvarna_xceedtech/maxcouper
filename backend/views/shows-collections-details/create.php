<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShowsCollectionsDetails */

$this->title = 'Shows Collections Details';
$this->params['breadcrumbs'][] = ['label' => 'Shows Collections Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shows-collections-details-create">

  <!--   <h1><?= Html::encode($this->title) ?></h1>
 -->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
