<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShowsCollectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shows Collections';
$this->params['breadcrumbs'][] = $this->title;
$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'multipledelete\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>

<!-- <div class="resources-index">
    <div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab">Shows Collection</a></li>
    <li><a href="<?= \yii\helpers\Url::to(['shows-collection-details/index']);?>" >Shows Collection Details</a></li>
  
  </ul>
<br> -->
<div class="podcast-index">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i>  Shows Collection List</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
               <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
               <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?>
            </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                 [
                    'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                            return ['value' => $data->id];
                    },
                ],   
                [
                  'attribute'=>'title',
                  'format' => 'raw',
                  'value'=>function($data) {
                      // return Html::a(Html::encode($model->title),['your-journey/view?id='.$model['id']]);
                    return '<p>'.$data->title.'</p>';
                  },
                ],
                // 'description:ntext',
                 
                 [
                  'attribute'=>'collection_id',
                  'format' => 'raw',
                  'value'=>function($data) {
                      // return Html::a(Html::encode($model->title),['your-journey/view?id='.$model['id']]);
                    //$category_name=Category::find()->where(['status'=>1,'id'=>$data->category_id])->one();
                     //print_r($itemtenders['title']);die;
                   // return '<p>'.$category_name->title.'</p>';
                    return '';
                  },
                ],

               
                
                [
                    'header'=>'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                       return Html::a('<i class="fa fa-pencil"></i>', ['shows-collection/update?id='.$model['id']]);
                      // return Html::a('<i class="fa fa-trash"></i>', ['podcast/delete?id='.$model['id'].'&flag=1'], ['data-method'=>'POST']) ;                    
                    },
                ],
            ],
        ]); ?>
      </div>
    </div>

</div>
<!-- </div></div> -->