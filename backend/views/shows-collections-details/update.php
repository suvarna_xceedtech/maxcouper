<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShowsCollectionsDetails */

$this->title = 'Update Shows Collections Details: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Shows Collections Details', 'url' => ['shows-collection/index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shows-collections-details-update">

   <!--  <h1><?= Html::encode($this->title) ?></h1>
 -->
    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
