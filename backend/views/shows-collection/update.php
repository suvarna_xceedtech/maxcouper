<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShowsCollections */

$this->title = 'Shows Collections ';
$this->params['breadcrumbs'][] = ['label' => 'Shows Collections', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shows-collections-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('update_form_new', [
        'model' => $model,
    ]) ?>

</div>
