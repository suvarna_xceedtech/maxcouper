<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;
use backend\models\Category;


$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
/* @var $this yii\web\View */
/* @var $model backend\models\Podcast */
/* @var $form yii\widgets\ActiveForm */
$itemtenders=Category::find()->where(['status'=>1])->all();
     $category_name=ArrayHelper::map($itemtenders,'id','title');
?>

<!------ Include the above in your HEAD tag ---------->
<head>
<style type="text/css">
 .table>tfoot>tr>td {
    padding:30px !important;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 0px solid #ddd !important;
    text-align: center;
}
th {
    text-align: center !important;
}

</style>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
            <script src="http://cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>

<div class="podcast-form">
    <div class="card">
        <div class="card-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
              <div class="box-header with-border">
                <div class="col-md-8">
                    <h3 class="box-title"> <i class="fa fa-pencil"></i> Add Shows Collections</h3>
                </div>
                <div class="col-md-4">
                    <div class="pull-right">
                        <?= Html::submitButton('Save', ['id'=>'submit_upper','class' => 'btn btn-success']) ?>
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                         <div class="form-group ">
                         <?= $form->field($model, 'category_id')->dropDownList($category_name,['prompt'=>'Select Category '])?>
                       </div></div>
                       
                       <div class="col-md-6">
                       <div class="form-group">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                            
                        </div></div>

                        <!-- <div class="col-md-6">
                       <div class="form-group">
                            <?= $form->field($model, 'sub_title')->textInput(['maxlength' => true]) ?>
                            
                        </div></div> -->
                        <div class="col-md-6">
                       <div class="form-group">
                            <?= $form->field($model, 'sequence')->textInput(['maxlength' => true]) ?>
                            
                        </div></div>

                      </div>
                        
                         <div class="row">
                    <div class="col-md-12">
                       <table id="myTable" class="table order-list" style=" border-style:solid;border-width: thin;
                border-collapse: collapse;">
                        <thead>
        
                     <tr>
                       <th colspan="8">Show Collection Details</th>
                      </tr>

                      <tr>
          

                      <!--  <th>Title</th> -->
                        <th>Image</th>
                       <th>Image Alt</th>
                     <th>Left Description</th>
                      <th>Bottom Description</th>
                      <th>Order</th>
                 </tr>
                 </thead>
                    <tbody>
                      <div class="row">
        <tr>
            
            
           <!--  <td class="col-md-2">
                
                        
                         <input type="text" name="title1"  id='title1'  class="form-control"required/>
                    
            </td> -->
            <!-- <td class="col-md-2">
                
               
              <textarea name="sub_description"  id='sub_description1' class="form-control" required></textarea>
            </td> -->
            <td class="col-md-2">

                      
               <input type="file" name="image1" id='image1' class="form-control"  />
            </td>
            <td class="col-md-2">
                
                <input type="text" name="image_alt1" id='image_alt1' class="form-control" />
            </td>
            
            <td class="col-md-4">
                
               
              <textarea name="left_description1"  id='left_description1' class="form-control ckeditor" ></textarea>
            </td>
            <td class="col-md-4">

                
               <!--  <input type="textarea" name="bottom_description1" id='bottom_description1' class="form-control" required/> -->
                <textarea  name="bottom_description1" id='bottom_description1' class="form-control ckeditor" ></textarea>
            </td>

            <td class="col-md-2">

                      
               <input type="text"  title="Order Should Be Integer Number" name="sort1" id='sort1' class="form-control"  />
            </td>
            
            <input type="hidden" name="rowcount" id="rowcount" value="1" />
            <td class="col-md-1"><a class="deleteRow"><i class="fa fa-trash"></i></a>

            </td>
        
        </tr>
        </div> 
                    </tbody>


                    <tfoot>
        <tr><center>
            <td  style="text-align: left;">
            <input type="button" class="btn btn-success btn-block " id="addrow" value="Add Show" />
                
                 
            </td>

        </center>
        </tr>
        <tr>
        </tr>
    </tfoot>
</table>
                 
                        
                       

                     </div>   
                         
                    </div>
                   
                </div>


              </div>
              <div class="box-footer">
                <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
              </div>

        </div>       
            <?php ActiveForm::end(); ?>
    </div>
</div>
    <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog" style="min-width:630px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style=" margin-top:30px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="padding-top:30px;">
                       
                          <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
   function fileuploadHandler(e, data){
     console.log('upload done');
     console.log( data._response.result.files[0].url);
      
   }

   </script>
<script type="text/javascript">


// CKEDITOR.replaceAll('ckeditor_description', {
//           height: 100,

//         // set toolbar that you want to show
//      removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
//     toolbar: [
//         { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink'] },
        
//        {name: 'colors', items : ['TextColor', 'BGColor']},
//         { name: 'styles', items: [ 'Format'] },

//     ],
//     toolbarGroups: [
       
//         { name: 'group1', groups: [ 'basicstyles' ] },
//     ],
//     removePlugins: 'elementspath',
//     removeDialogTabs: 'link:advanced;image:Link;image:advanced',
//             } );


  
$("#submit_lower").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$("#submit_upper").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
function validateExtension(id) {

    //Get reference of FileUpload.
    var fileUpload = document.getElementById(id);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.mp3)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 10) { // make it in MB so divide by 1024*1024
                    alert('Please select audio file size less than 10 MB');
                    return false;
                    }
                    else
                    {
                      var height = this.height;
                      var width = this.width;
                      return true;
                    }
                    
                   
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid audio file.");
        fileUpload.value="";
        return false;
    }
}
</script>

<!-- <script>
    $('.ckeditor_description').each(function () {
        CKEDITOR.replace($(this).prop('id'));
    });
</script> -->
<script>

    $(document).ready(function () {


    //var counter = 1;
     var rowcount=$('#rowcount').val();
   // var rowcount = 0;

    $("#addrow").on("click", function () {
         rowcount=parseInt(rowcount)+1;
       // var newRow = $("<tr id='+'>");
       
    //alert(counter);
        var cols = "";
        // var script_per_week= $("#script_per_week").val();
        //  var one_month= $("#one_month").val();
        //  var two_month=$("#two_month").val();
        //  var three_month=$("#three_month").val();
        //  alert(script_per_week);
      
        cols += '<tr id="'+ rowcount +'">';

        
        cols += '<td><input type="file" class="form-control" name="image' + rowcount + '" id="image' + rowcount + '" required/></td>';
        
         cols += '<td><input type="text" class="form-control" name="image_alt' + rowcount + '" id="image_alt' + rowcount + '" required/></td>';

        cols += '<td> <textarea  name="left_description' + rowcount + '" id="left_description' + rowcount + '"class="form-control ckeditor" required></textarea></td>';

        cols += '<td> <textarea name="bottom_description' + rowcount + '" id="bottom_description' + rowcount + '" class="form-control ckeditor" required></textarea></td>';

         cols += '<td><input type="text"  title="Order Should Be Integer Number" name="sort' + rowcount + '" id="sort' + rowcount + '" class="form-control"  required/></td>';
       
          
        cols += '<td><a class="ibtnDel" title="Delete"><i class="fa fa-trash"></i></a></td><tr>';
        

       

        $("#rowcount").val(rowcount);
       // newRow.append(cols);
      // counter++;
        $("tbody").append(cols);

    //      $('.ckeditor_description').each(function () {
    //     CKEDITOR.replace($(this).prop('id'));
    // });
    CKEDITOR.replace("left_description"+rowcount+"");
    CKEDITOR.replace("bottom_description"+rowcount+"");
        
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        //counter -= 1
        rowcount=parseInt(rowcount)-1;

        $("#rowcount").val(rowcount);
    });


});

</script>

</div>
