<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "home_slides".
 *
 * @property int $id
 * @property int|null $home_main_id
 * @property string|null $image
 * @property string|null $image_alt
 * @property string|null $title
 * @property string|null $title_position
 * @property string|null $top_description
 * @property string|null $middle_description
 * @property string|null $bottom_description
 * @property int $sequence
 * @property string|null $meta_title
 * @property string|null $meta_keyword
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int $status
 * @property int|null $created_by
 * @property string|null $alias
 */
class HomeSlides extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'home_slides';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sequence', 'bottom_description','btm_text_horz_position','btm_text_vert_position'], 'required'],
            [['home_main_id', 'sequence', 'updated_by', 'status', 'created_by'], 'integer'],
            [['top_description', 'middle_description', 'bottom_description'], 'string'],
            [['sequence'], 'number'],
            [['created_at', 'updated_at','btm_text_horz_position','btm_text_vert_position','title_position_vert'], 'safe'],
            [['image', 'image_alt', 'title', 'title_position','title_position_vert', 'alias'], 'string', 'max' => 200],
            [['meta_title', 'meta_keyword'], 'string', 'max' => 100],
             [['image'], 'required', 'on' => 'create'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'home_main_id' => 'Home Main ID',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'title' => 'Title',
            'title_position' => 'Title Position Horizontal',
            'title_position_vert'=> 'Title Position Vertical',
            'top_description' => 'Top Description',
            'middle_description' => 'Middle Description',
            'bottom_description' => 'Bottom Description',
            'sequence' => 'Sequence',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'created_by' => 'Created By',
            'alias' => 'Alias',
            'btm_text_horz_position'=>'Bottom Text Horizontal Position',
            'btm_text_vert_position'=>'Bottom Text Vertical Position',
        ];
    }
}
