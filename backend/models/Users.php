<?php

namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $status
 */
class Users extends \yii\db\ActiveRecord 
{
    /**
     * {@inheritdoc}
     */
    public $oldpass;
    public $newpass;
    public $repeatnewpass;
    public $fullname;
    public $first_name;
    const SCENARIO_REPORT = 'report';
    const SCENARIO_CREATE = 'create';
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           [['first_name','email', 'password', ], 'required',  'on' => self::SCENARIO_CREATE],
            [['starsign','birth_date'], 'safe'],
            [['email'], 'unique', 'on' => self::SCENARIO_CREATE],
           // [['email'], 'unique', 'targetAttribute' => 'status'],


            [['email_id'], 'UniqueRules', 'on' => 'resetpassword'],
            [['oldpass', 'newpass', 'repeatnewpass'], 'required', 'on' => 'resetpassword'],
           // ['repeatnewpass','compare','compareAttribute'=>'newpass'],
            [['status'], 'integer'],
            [['username', 'password'], 'string', 'max' => 100],
            ['email', 'email'],
            [['first_name','last_name','user_id','email_id', 'password', 'created_at', 'fullname'],'safe'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'status' => 'Status',
            'oldpass'=>'Old Password',
            'newpass'=>'New Password',
            'repeatnewpass'=>'Confirm Password',
            'user_id' => 'User',
            'email_id' => 'Email/Username',
            'star_sign' => 'Star Sign',
            'birth_date' => 'Birth Date',
            'first_name' => 'Name'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    public static function getUsers()
    {
        return parent::findAll(['status' =>1]);
   }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

  
    public function getName()
    {
        return Yii::$app->user->identity->name;
    }

    public function UniqueRules()
    {
        if(!empty($this->email)){
            $unique=User::find()->where(['email'=>$this->email])->count();

            if($unique>0)
            {
                $this->addError('email','Email already exists');
            }
            
        }
    }

    public function UniqueUsernameRules()
    {
        
            $query=new Query;
            $query->from('user')
                    ->where(['email' => $this->email, 'status'=>1]);
            $command = $query->createCommand();
            $User = $command->queryOne(); 
            
            if($User!="" && $this->id!=$User['id'])
            {
                $this->addError('email','Email must be unique');
            }
    }

    
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
