<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\HomeSlides;

/**
 * HomeSlidesSearch represents the model behind the search form of `app\models\HomeSlides`.
 */
class HomeSlidesSearch extends HomeSlides
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'home_main_id', 'sequence', 'updated_by', 'status', 'created_by'], 'integer'],
            [['image', 'image_alt', 'title', 'title_position', 'title_position_vert'
            ,'top_description', 'middle_description', 'bottom_description', 'meta_title', 'meta_keyword', 'created_at', 'updated_at', 'alias','btm_text_horz_position','btm_text_vert_position'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomeSlides::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'home_main_id' => $this->home_main_id,
            'sequence' => $this->sequence,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'title_position', $this->title_position])
            ->andFilterWhere(['like', 'top_description', $this->top_description])
            ->andFilterWhere(['like', 'middle_description', $this->middle_description])
            ->andFilterWhere(['like', 'bottom_description', $this->bottom_description])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keyword', $this->meta_keyword])
            ->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}
