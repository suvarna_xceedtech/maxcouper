<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $copyright
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $google_analytics
 * @property string $robots
 * @property string $sitemap
 * @property int $status
 * @property string|null $created_at
 * @property int $created_by
 * @property string|null $updated_at
 * @property int $updated_by
 * @property string $alias
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright','site_title'], 'required'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['copyright', 'facebook', 'twitter', 'instagram', 'google_analytics', 'site_title', 'logo_text'], 'string', 'max' => 200],
            [['robots', 'sitemap'], 'string', 'max' => 300],
            [['robots'], 'required', 'on' => 'create'],
            [['sitemap'], 'required', 'on' => 'create'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'copyright' => 'Copyright',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'google_analytics' => 'Google Analytics',
            'robots' => 'Robots',
            'sitemap' => 'Sitemap',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'site_title' => 'Site Title',
            'logo_text' => 'Logo Text',
        ];
    }
}
