<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "shows_collections_details".
 *
 * @property int $id
 * @property int $collection_id
 * @property string $title
 * @property string $leftside_description
 * @property string $bottom_description
 * @property string $image
 * @property string $image_alt
 * @property int $sort
 * @property string $alias
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string|null $created_at
 * @property int $created_by
 * @property string|null $updated_at
 * @property int $updated_by
 * @property int $status
 */
class ShowsCollectionsDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shows_collections_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           [['collection_id','sort'], 'required'],
            [['collection_id', 'sort', 'created_by', 'updated_by', 'status'], 'integer'],
            [['sort'], 'number'],
       
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'image', 'alias', 'meta_title', 'meta_keyword', 'meta_description'], 'string', 'max' => 200],
            [['image_alt'], 'string', 'max' => 100],
             [['image'], 'required', 'on' => 'create'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'collection_id' => 'Collection',
            'title' => 'Title',
            'leftside_description' => 'Left Description',
            'bottom_description' => 'Bottom Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'sort' => 'Sequence',
            'alias' => 'Alias',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
}
