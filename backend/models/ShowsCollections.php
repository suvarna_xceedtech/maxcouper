<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "shows_collections".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $alias
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string|null $created_at
 * @property int $created_by
 * @property string|null $updated_at
 * @property int $updated_by
 * @property int $status
 */
class ShowsCollections extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $leftside_description;
    public $bottom_description;
    public $image_alt;
    public $image;
    public $sort;       

    public static function tableName()
    {
        return 'shows_collections';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'title','sequence'], 'required'],
            [['category_id', 'created_by', 'updated_by', 'status','sort','sequence'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
             [['sort'], 'number'],
            [['title', 'alias', 'meta_title', 'meta_keyword', 'meta_description'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'title' => 'Collection Name',
            'sub_title' =>'Sub Title',
            'alias' => 'Alias',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'sequence'=>'Sequence'
        ];
    }
}
