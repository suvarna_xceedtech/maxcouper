<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category_details".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $title
 * @property string|null $image
 * @property string|null $short_text
 * @property string|null $detail_text
 */
class CategoryDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','sequence'], 'required'],
            [['category_id','sequence'], 'integer'],
            [['detail_text'], 'string'],
            [['title', 'image'], 'string', 'max' => 200],
            [['short_text'], 'string', 'max' => 2000],
            [['image'], 'required', 'on' => 'create'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'image' => 'Image',
            'short_text' => 'Short Text',
            'detail_text' => 'Details Text',
            'sequence'=>'Sequence'
        ];
    }
}
