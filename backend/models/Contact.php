<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $alt
 * @property string $description
 * @property string $alias
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property int $status
 * @property string $email
 * @property string|null $created_at
 * @property int $created_by
 * @property string|null $updated_at
 * @property int $updated_by
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description','contact_email','subtitle'], 'required'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'alt', 'alias', 'meta_title', 'meta_keyword', 'meta_description', 'contact_email'], 'string', 'max' => 200],
            [['image'], 'string', 'max' => 300],
            [['description'], 'string', 'max' => 1000],
            ['contact_mobile', 'match', 'pattern' => '/^\d{10}$/', 'message' => 'Please enter a valid Mobile number.'],
             ['contact_email','email','message'=>'Email ID is not valid'],
             [['image'], 'required', 'on' => 'create'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'alt' => 'Alt',
            'description' => 'Details',
            'alias' => 'Alias',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'status' => 'Status',
            'contact_email' => 'Admin Email',
            'subtitle'=>'Sub Title',
            'contact_mobile'=>'Mobile',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
