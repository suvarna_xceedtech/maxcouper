<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "home_main".
 *
 * @property int $id
 * @property string|null $logo_text
 * @property string|null $home-image1
 * @property string|null $home_image2
 * @property string|null $home_intro
 * @property string|null $alias
 * @property string|null $meta_title
 * @property string|null $meta_keyword
 * @property string|null $meta_description
 * @property int $status
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 */
class HomeMain extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'home_main';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
             [['title','home_intro','home_image1','home_image2'], 'required'],
            [['title', 'home_intro'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','meta_title','meta_keyword','meta_description'], 'safe'],
            [['home_image1', 'home_image2', 'alias', 'meta_title', 'meta_keyword', 'meta_description'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'home_image1' => 'Home Image 1',
            'home_image2' => 'Home Image 2',
            'home_intro' => 'Home Introduction',
            'alias' => 'Alias',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
