<?php
namespace common\extensions;

//require_once 'vendor/stripe/autoload.php';
require_once('Stripe/init.php');


use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\ApiOperations\Create;
use \Stripe\Charge;
use \Stripe\Token;

class StripePayment
{

    private $apiKey;

    private $stripeService;

    public function __construct()
    {
        require_once "config.php";
        $this->apiKey = STRIPE_SECRET_KEY;
        $this->stripeService = new \Stripe\Stripe();
        $this->stripeService->setVerifySslCerts(false);
        $this->stripeService->setApiKey($this->apiKey);
    }

    public function addCustomer($customerDetailsAry)
    {
        
        $customer = new Customer();
        
        $customerDetails = $customer->create($customerDetailsAry);
        
        return $customerDetails;
    }
        public function createToken($cardDetails)
        {
          try {
                 $result = Token::create(
                            array(
                                "card" => array(
                                    "name" => $cardDetails['name'],
                                    "number" => $cardDetails['card-number'],
                                    "exp_month" => $cardDetails['month'],
                                    "exp_year" => $cardDetails['year'],
                                    "cvc" => $cardDetails['cvc']
                                )
                            )
                        );
                        $token = $result['id'];
                        $resultData=array();
                        $resultData['status']=200;
                        $resultData['token']=$token;
                        return $resultData;
                      
                      } catch(\Stripe\Exception\CardException $e) {
                        $resultData=array();
                        $resultData['status']=502;
                        $resultData['token']=$e->getError()->message;
                        return $resultData;
                      }
        }
    public function chargeAmountFromCard($cardDetails)
    {        if($cardDetails['stripe_customer_id']=='0'){
                $customerDetailsAry = array(
                    'email' => $cardDetails['email'],
                    'source' => $cardDetails['token']
                );
              }
              else
              {
                      /* $result = Token::create(
                            array(
                                "card" => array(
                                    //"name" => $cardDetails['name'],
                                    "number" => $cardDetails['stripe_customer_code'],
                                    //"exp_month" => $cardDetails['month'],
                                    //"exp_year" => $cardDetails['year'],
                                    "cvc" => $cardDetails['cvc']
                                )
                            )
                        );
                        $token = $result['id'];*/ 
                        $customerDetailsAry = array(
                          'email' => $cardDetails['email'],
                          //'source' => $token
                     ); 
              }
          try {
            if($cardDetails['stripe_customer_id']=='0'){
             $customerResult = $this->addCustomer($customerDetailsAry);
              $cust_id=$customerResult->id;
             // $code=$customerResult->sources->data[0]->last4;
            }
            else
            { 
              $cust_id=$cardDetails['stripe_customer_id'];
              $code=$cardDetails['stripe_customer_code'];
            }

              $charge = new Charge();             
        $cardDetailsAry = array(
            'customer' => $cust_id,
            'amount' => $cardDetails['amount']*100 ,
            'currency' => $cardDetails['currency_code'],
            'metadata' => array(
            )
        );
        $resultData=array();
        $result = $charge->create($cardDetailsAry);
        $code=$result->source->last4;
        $resultData['status']=200;
        $resultData['customer_id']=$cust_id;
         $resultData['customer_code']=$code;
        $resultData['data']=$result->jsonSerialize();        
        return $resultData;
  // Use Stripe's library to make requests...
} catch(\Stripe\Exception\CardException $e) {
     $resultData=array();
     $resultData['status']=502;
        $resultData['data']=$e->getError()->message;
  // Since it's a decline, \Stripe\Exception\CardException will be caught
  //echo 'Status is:' . $e->getHttpStatus() . '\n';
 // echo 'Type is:' . $e->getError()->type . '\n';
 // echo 'Code is:' . $e->getError()->code . '\n';
  // param is '' in this case
 // echo 'Param is:' . $e->getError()->param . '\n';
  //echo 'Message is:' . $e->getError()->message . '\n';
    return $resultData;
} catch (\Stripe\Exception\RateLimitException $e) {
  // Too many requests made to the API too quickly

} catch (\Stripe\Exception\InvalidRequestException $e) {
  // Invalid parameters were supplied to Stripe's API
} catch (\Stripe\Exception\AuthenticationException $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
} catch (\Stripe\Exception\ApiConnectionException $e) {
  // Network communication with Stripe failed
} catch (\Stripe\Exception\ApiErrorException $e) {
  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {
  // Something else happened, completely unrelated to Stripe
}
       
    }
}
